package JavaDSA.Workshops.LinkedList.Skeleton.src.com.company.dsa;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList<T> implements List<T> {
    private Node head;
    private Node tail;
    private int size = 0;

    public LinkedList() {
        this.head = null;
        this.tail = null;
    }

    public LinkedList(Iterable<T> iterable) {
        iterable.forEach(this::addLast);
    }

    @Override
    public void addFirst(T value) {
        Node newNode = new Node(value);
        newNode.next = this.head;
        this.head = newNode;
        if (size == 0) {
            this.tail = newNode;
        }
        this.size++;
    }

    @Override
    public void addLast(T value) {
        Node newNode = new Node(value);
        if (size == 0) {
            this.head = newNode;
            this.tail = newNode;
            this.size++;
            return;
        }
        this.tail.next = newNode;
        newNode.prev = this.tail;
        this.tail = newNode;
        this.size++;
    }

    @Override
    public void add(int index, T value) {
        if (index > size || index < 0) {
            throw new NoSuchElementException();
        }

        if (index == 0) {
            addFirst(value);
        } else if (index == size) {
            addLast(value);
        } else {
            Node newNode = new Node(value);
            Node nodeAtIndex = getNodeByIndex(index);
            newNode.next = nodeAtIndex;
            newNode.prev = nodeAtIndex.prev;
            nodeAtIndex.prev.next = newNode;
            nodeAtIndex.prev = newNode;
            size++;
        }
    }

    @Override
    public T getFirst() {
        throwIfEmpty();
        return this.head.value;
    }

    @Override
    public T getLast() {
        throwIfEmpty();
        return this.tail.value;
    }

    @Override
    public T get(int index) {
        return getNodeByIndex(index).value;
    }

    @Override
    public int indexOf(T value) {
        Node x = this.head;
        for (int index = 0; index < size; index++) {
            if (x.value == value) {
                return index;
            } else {
                x = x.next;
            }
        }
        return -1;
    }

    @Override
    public T removeFirst() {
        throwIfEmpty();
        T result = this.head.value;
        this.head = this.head.next;
        size--;
        return result;
    }

    @Override
    public T removeLast() {
        throwIfEmpty();
        if (size == 1) {
            T result = this.tail.value;
            this.head = null;
            this.tail = null;
            this.size--;
            return result;
        }
        T result = this.tail.value;
        this.tail = this.tail.prev;
        this.tail.next = null;
        size--;
        return result;
    }


    @Override
    public int size() {
        return this.size;
    }

    @Override
    public Iterator<T> iterator() {
        return new linkedListIterator(this.head);
    }

    private Node getNodeByIndex(int index) {
        throwIfIndexIsInvalid(index);
        Node x;
        if (size - index >= size / 2) {
            x = this.head;
            for (int i = 0; i < index; i++) {
                x = x.next;
            }
        } else {
            x = this.tail;
            for (int i = size - 1; i >= index; i--) {
                x = x.prev;
            }
        }
        return x;
    }

    private void throwIfEmpty() {
        if (size == 0) {
            throw new NoSuchElementException();
        }
    }

    private void throwIfIndexIsInvalid(int index) {
        if (index >= size || index < 0) {
            throw new NoSuchElementException();
        }
    }

    private class Node {
        T value;
        Node prev;
        Node next;

        Node(T value) {
            this.value = value;
        }
    }


    private class linkedListIterator implements Iterator<T> {
        private Node current;

        public linkedListIterator(Node head) {
            current = head;
        }

        @Override
        public boolean hasNext() {
            return this.current.next != null;
        }

        @Override
        public T next() {
            T result = this.current.value;
            this.current = this.current.next;
            return result;
        }
    }
}
