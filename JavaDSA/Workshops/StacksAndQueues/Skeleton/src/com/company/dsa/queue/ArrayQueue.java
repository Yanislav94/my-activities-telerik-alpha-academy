package JavaDSA.Workshops.StacksAndQueues.Skeleton.src.com.company.dsa.queue;

import java.util.NoSuchElementException;

public class ArrayQueue<E> implements Queue<E> {
    private static final int DEFAULT_CAPACITY = 4;
    private E[] items;
    private int head, tail, size;
    private int capacity;

    public ArrayQueue() {
        this.items = (E[]) new Object[DEFAULT_CAPACITY];
        this.head = 0;
        this.tail = -1;
        this.size = 0;
        this.capacity = DEFAULT_CAPACITY;
    }

    public ArrayQueue(int capacity) {
        this.items = (E[]) new Object[capacity];
        this.head = 0;
        this.tail = -1;
        this.size = 0;
        this.capacity = capacity;
    }

    @Override
    public void enqueue(E element) {
        if (tail == capacity - 1) {
            this.resize();
        }
        this.items[++tail] = element;
        size++;
    }

    @Override
    public E dequeue() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        size--;
        return this.items[head++];
    }

    @Override
    public E peek() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return this.items[head];
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    private void resize() {
        this.capacity *= 2;
        E[] resizedItems = (E[]) new Object[this.capacity];
        System.arraycopy(this.items, 0, resizedItems, this.head, this.tail - this.head);
        this.tail -= this.head;
        this.head = 0;
        this.items = resizedItems;
    }
}
