package JavaDSA.Workshops.StacksAndQueues.Skeleton.src.com.company.dsa.stack;

import JavaDSA.Workshops.StacksAndQueues.Skeleton.src.com.company.dsa.Node;

import java.util.NoSuchElementException;

public class LinkedStack<E> implements Stack<E> {
    private Node<E> top;
    private int size;

    public LinkedStack() {
        this.top = null;
        this.size = 0;
    }

    @Override
    public void push(E element) {
        Node<E> temp = new Node<>(element);
        temp.next = top;
        this.top = temp;
        size++;
    }

    @Override
    public E pop() {
        throwIfEmpty();
        E result = this.top.data;
        this.top = this.top.next;
        size--;
        return result;
    }

    @Override
    public E peek() {
        throwIfEmpty();
        return this.top.data;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    private void throwIfEmpty() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
    }
}
