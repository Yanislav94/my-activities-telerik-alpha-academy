package JavaDSA.Workshops.StacksAndQueues.Skeleton.src.com.company.dsa.queue;


import JavaDSA.Workshops.StacksAndQueues.Skeleton.src.com.company.dsa.Node;

import java.util.NoSuchElementException;

public class LinkedQueue<E> implements Queue<E> {
    private Node<E> head, tail;
    private int size;

    public LinkedQueue() {
        this.size = 0;
        this.head = null;
        this.tail = null;
    }

    @Override
    public void enqueue(E element) {
        Node<E> temp = new Node<>(element);
        if (isEmpty()) {
            this.tail = temp;
            this.head = temp;
        } else {
            this.tail.next = temp;
            this.tail = temp;
        }
        size++;
    }

    @Override
    public E dequeue() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        E result = this.head.data;
        this.head = this.head.next;
        size--;
        return result;
    }

    @Override
    public E peek() {
        if (size == 0) {
            throw new NoSuchElementException();
        }
        return this.head.data;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }
}
