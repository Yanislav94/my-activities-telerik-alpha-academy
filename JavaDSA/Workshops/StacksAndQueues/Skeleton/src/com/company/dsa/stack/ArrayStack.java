package JavaDSA.Workshops.StacksAndQueues.Skeleton.src.com.company.dsa.stack;

import java.util.NoSuchElementException;

public class ArrayStack<E> implements Stack<E> {
    private static final int DEFAULT_CAPACITY = 4;
    private E[] items;
    private int size;
    private int top;
    private int capacity;

    public ArrayStack() {
        this.items = (E[]) new Object[DEFAULT_CAPACITY];
        this.size = 0;
        this.top = -1;
        this.capacity = DEFAULT_CAPACITY;
    }

    public ArrayStack(int capacity) {
        this.items = (E[]) new Object[capacity];
        this.size = 0;
        this.top = -1;
        this.capacity = capacity;
    }

    @Override
    public void push(E element) {
        if (top == this.size - 1) {
            this.resize();
        }
        this.items[++top] = element;
        this.size++;
    }


    @Override
    public E pop() {
        if (top < 0) {
            throw new NoSuchElementException();
        }
        this.size--;
        return this.items[top--];
    }

    @Override
    public E peek() {
        if (top < 0) {
            throw new NoSuchElementException();
        }
        return this.items[top];
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    private void resize() {
        this.capacity *= 2;
        E[] resizedItems = (E[]) new Object[this.capacity];
        System.arraycopy(this.items, 0, resizedItems, 0, this.items.length);
        this.items = resizedItems;
    }
}
