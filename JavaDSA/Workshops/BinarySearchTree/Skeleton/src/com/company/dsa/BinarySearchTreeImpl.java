package JavaDSA.Workshops.BinarySearchTree.Skeleton.src.com.company.dsa;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BinarySearchTreeImpl<E extends Comparable<E>> implements BinarySearchTree<E> {
    private E value;
    private BinarySearchTreeImpl<E> left;
    private BinarySearchTreeImpl<E> right;

    public BinarySearchTreeImpl(E value) {
        this.value = value;
        left = null;
        right = null;
    }

    @Override
    public E getRootValue() {
        return value;
    }

    @Override
    public BinarySearchTree<E> getLeftTree() {
        return left;
    }

    @Override
    public BinarySearchTree<E> getRightTree() {
        return right;
    }

    @Override
    public void insert(E value) {
        if (value.compareTo(this.value) > 0) {
            if (this.right != null) {
                this.right.insert(value);
            } else {
                this.right = new BinarySearchTreeImpl<>(value);
            }
        } else {
            if (this.left != null) {
                this.left.insert(value);
            } else {
                this.left = new BinarySearchTreeImpl<>(value);
            }
        }
    }

    @Override
    public boolean search(E value) {
        return searchValue(this, value);
    }

    private boolean searchValue(BinarySearchTreeImpl<E> node, E value) {
        if (node == null) {
            return false;
        }
        if (node.value.equals(value)) {
            return true;
        }
        return searchValue(node.left, value) || searchValue(node.right, value);
    }

    @Override
    public List<E> inOrder() {
        return inOrder(this, new ArrayList<>());
    }

    private List<E> inOrder(BinarySearchTreeImpl<E> node, ArrayList<E> resultList) {
        if (node.left != null) {
            inOrder(node.left, resultList);
        }
        resultList.add(node.value);
        if (node.right != null) {
            inOrder(node.right, resultList);
        }
        return resultList;
    }


    @Override
    public List<E> postOrder() {
        return postOrder(this, new ArrayList<>());
    }

    private List<E> postOrder(BinarySearchTreeImpl<E> node, ArrayList<E> resultList) {
        if (node.left != null) {
            postOrder(node.left, resultList);
        }
        if (node.right != null) {
            postOrder(node.right, resultList);
        }
        resultList.add(node.value);
        return resultList;
    }

    @Override
    public List<E> preOrder() {
        return preOrder(this, new ArrayList<>());
    }

    private List<E> preOrder(BinarySearchTreeImpl<E> node, ArrayList<E> resultList) {
        resultList.add(node.value);
        if (node.left != null) {
            preOrder(node.left, resultList);
        }
        if (node.right != null) {
            preOrder(node.right, resultList);
        }
        return resultList;
    }

    @Override
    public List<E> bfs() {
        return bfs(this, new ArrayList<>(), new LinkedList<>());
    }

    private List<E> bfs(BinarySearchTreeImpl<E> node, ArrayList<E> resultList, Queue<BinarySearchTreeImpl<E>> queue) {
        queue.offer(node);
        while (!queue.isEmpty()) {
            BinarySearchTreeImpl<E> next = queue.remove();
            resultList.add(next.value);

            if (next.left != null) {
                queue.offer(next.left);
            }
            if (next.right != null) {
                queue.offer(next.right);
            }
        }
        return resultList;
    }

    @Override
    public int height() {
        return getHeight(this);
    }

    private int getHeight(BinarySearchTreeImpl<E> node) {
        if (node.left == null && node.right == null) {
            return 0;
        } else {

            int lDepth = 0;
            if (node.left != null) {
                lDepth = getHeight(node.left);
            }

            int rDepth = 0;
            if (node.right != null) {
                rDepth = getHeight(node.right);
            }

            if (lDepth > rDepth)
                return (lDepth + 1);
            else
                return (rDepth + 1);
        }
    }

    //     Advanced task: implement remove method. To test, uncomment the commented tests in BinaryTreeImplTests
    @Override
    public boolean remove(E value) {
        BinarySearchTreeImpl<E> resultNode = deleteRec(this, value);
        return resultNode.value == value;
    }

    BinarySearchTreeImpl<E> deleteRec(BinarySearchTreeImpl<E> root, E value) {
        if (root == null)
            return root;

        if (value.compareTo(root.value) < 0)
            root.left = deleteRec(root.left, value);
        else if (value.compareTo(root.value) > 0)
            root.right = deleteRec(root.right, value);

        else {

            if (root.left == null)
                return root.right;
            else if (root.right == null)
                return root.left;

            root.value = minValue(root.right);

            root.right = deleteRec(root.right, root.value);
        }
        return root;
    }

    E minValue(BinarySearchTreeImpl<E> root) {
        E minv = root.value;
        while (root.left != null) {
            minv = root.left.value;
            root = root.left;
        }
        return minv;
    }
}
