package JavaDSA.CodingTasks.Recursion.LargestSurface;

import java.util.Scanner;

public class Main {
    private static String[][] matrix;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        matrix = new String[sc.nextInt()][sc.nextInt()];
        sc.nextLine();
        populateMatrix(sc);
        int currentMax = 1;

        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[row].length; col++) {
                String currentStartElement = matrix[row][col];
                if (currentStartElement == null) {
                    continue;
                }
                int currentSequence = getCurrentSequence(row, col, currentStartElement);
                if (currentSequence > currentMax) {
                    currentMax = currentSequence;
                }
            }
        }
        System.out.println(currentMax);
    }

    private static int getCurrentSequence(int row, int col, String target) {
        if (!isInside(row, col)) {
            return 0;
        }
        if (matrix[row][col] == null) {
            return 0;
        }
        if (!matrix[row][col].equals(target)) {
            return 0;
        } else {
            int sum = 1;
            matrix[row][col] = null;
            sum += getCurrentSequence(row + 1, col, target);
            sum += getCurrentSequence(row - 1, col, target);
            sum += getCurrentSequence(row, col + 1, target);
            sum += getCurrentSequence(row, col - 1, target);
            return sum;
        }
    }

    private static void populateMatrix(Scanner sc) {
        for (int row = 0; row < matrix.length; row++) {
            matrix[row] = sc.nextLine().split("\\s+");
        }
    }

    private static boolean isInside(int row, int col) {
        return row >= 0 && row < matrix.length && col >= 0 && col < matrix[row].length;
    }

}
