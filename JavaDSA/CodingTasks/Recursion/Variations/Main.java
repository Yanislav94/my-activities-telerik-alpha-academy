package JavaDSA.CodingTasks.Recursion.Variations;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int length = Integer.parseInt(scanner.nextLine());

        String[] symbols = scanner.nextLine().split(" ");
        String X = symbols[0];
        String Y = symbols[1];

        List<String> variations = new ArrayList<>();
        generateVariations(length, X + Y, "", variations);

        Collections.sort(variations);

        for (String variation : variations) {
            System.out.println(variation);
        }
    }
    public static void generateVariations(int length, String symbols, String current, List<String> variations) {
        if (current.length() == length) {
            variations.add(current);
            return;
        }

        for (int i = 0; i < symbols.length(); i++) {
            generateVariations(length, symbols, current + symbols.charAt(i), variations);
        }
    }
}