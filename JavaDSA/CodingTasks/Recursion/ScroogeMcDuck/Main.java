package JavaDSA.CodingTasks.Recursion.ScroogeMcDuck;

import java.util.*;

public class Main {
    private static Short[][] matrix;
    private static byte playerRow;
    private static byte playerCol;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] dimensions = Arrays.stream(sc.nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();
        int n = dimensions[0];
        int m = dimensions[1];

        matrix = new Short[n][m];

        populateMatrix(sc);

        System.out.println(movePlayer());

    }

    private static int movePlayer() {
        if (!hasValidMove()) {
            return 0;
        } else {
            goToBestCoordinates();
            matrix[playerRow][playerCol]--;
            return movePlayer() + 1;
        }
    }

    private static boolean hasValidMove() {
        return
                isValidPosition(playerRow, playerCol - 1)
                        || isValidPosition(playerRow, playerCol + 1)
                        || isValidPosition(playerRow - 1, playerCol)
                        || isValidPosition(playerRow + 1, playerCol);
    }

    private static void goToBestCoordinates() {
        Map<String, Short> directionValues = new HashMap<>();

        if (isValidPosition(playerRow, playerCol - 1)) {
            directionValues.put("left", matrix[playerRow][playerCol - 1]);
        }
        if (isValidPosition(playerRow, playerCol + 1)) {
            directionValues.put("right", matrix[playerRow][playerCol + 1]);
        }
        if (isValidPosition(playerRow - 1, playerCol)) {
            directionValues.put("up", matrix[playerRow - 1][playerCol]);
        }
        if (isValidPosition(playerRow + 1, playerCol)) {
            directionValues.put("down", matrix[playerRow + 1][playerCol]);
        }

        String direction = directionValues
                .entrySet()
                .stream().min(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .get().getKey();

        switch (direction) {
            case "left":
                playerCol--;
                break;
            case "right":
                playerCol++;
                break;
            case "up":
                playerRow--;
                break;
            case "down":
                playerRow++;
                break;
        }
    }

    private static boolean isValidPosition(int row, int col) {
        return (row >= 0 && row < matrix.length && col >= 0 && col < matrix[row].length) && matrix[row][col] > 0;
    }

    private static void populateMatrix(Scanner sc) {
        for (byte row = 0; row < matrix.length; row++) {
            Short[] inputLine = Arrays.stream(sc.nextLine()
                            .split("\\s+"))
                    .map(Short::parseShort)
                    .toArray(Short[]::new);
            for (byte col = 0; col < matrix[row].length; col++) {
                Short currentCellValue = inputLine[col];
                matrix[row][col] = currentCellValue;
                if (currentCellValue == 0) {
                    playerRow = row;
                    playerCol = col;
                }
            }
        }
    }
}