package JavaDSA.CodingTasks.MapSetHashTables.Pokemons;

import java.util.*;
import java.util.stream.Collectors;

public class Main {
    private static Map<String, Set<Pokemon>> pokemonTypes;
    private static Set<Pokemon> pokemons;
    private static List<Pokemon> ranklist;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        pokemonTypes = new HashMap<>();
        pokemons = new HashSet<>();
        ranklist = new ArrayList<>();

        String input = sc.nextLine();
        while (!input.equals("end")) {
            String[] inputArgs = input.split("\\s+");
            String command = inputArgs[0];
            switch (command) {
                case "add":
                    System.out.println(addPokemon(inputArgs));
                    break;
                case "find":
                    System.out.println(findByType(inputArgs));
                    break;
                case "ranklist":
                    System.out.println(getRankList(inputArgs));
                    break;
            }
            input = sc.nextLine();
        }
    }

    private static String getRankList(String[] inputArgs) {
        int start = Integer.parseInt(inputArgs[1]);
        int end = Integer.parseInt(inputArgs[2]);
        StringBuilder builder = new StringBuilder();
        for (int i = start - 1; i < end; i++) {
            builder.append(String.format("%d. %s(%d)", i + 1, ranklist.get(i).name, ranklist.get(i).power));
            if (i < end - 1) {
                builder.append("; ");
            }
        }
        return builder.toString();
    }

    private static String findByType(String[] inputArgs) {
        String targetType = inputArgs[1];
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("Type %s: ", targetType));
        if (!pokemonTypes.containsKey(targetType)) {
            return builder.toString();
        }
        List<Pokemon> resultList = pokemonTypes.get(targetType).stream().collect(Collectors.toList());

        for (int i = 0; i < Math.min(resultList.size(), 5); i++) {
            builder.append(resultList.get(i));
            if (i < Math.min(resultList.size(), 5) - 1) {
                builder.append("; ");
            }
        }
        return builder.toString();
    }

    private static String addPokemon(String[] inputArgs) {
        String name = inputArgs[1];
        String type = inputArgs[2];
        int power = Integer.parseInt(inputArgs[3]);
        int position = Integer.parseInt(inputArgs[4]);
        Pokemon currentPokemon = new Pokemon(name, type, power);
        if (!pokemons.add(currentPokemon)) {
            return "";
        }
        if (!pokemonTypes.containsKey(type)) {
            pokemonTypes.put(type, new TreeSet<>());
        }
        pokemonTypes.get(type).add(currentPokemon);
        ranklist.add(position - 1, currentPokemon);
        return String.format("Added pokemon %s to position %d", name, position);
    }

    private static class Pokemon implements Comparable<Pokemon> {
        private String name;
        private String type;
        private int power;

        public Pokemon(String name, String type, int power) {
            this.name = name;
            this.type = type;
            this.power = power;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Pokemon pokemon = (Pokemon) o;
            return power == pokemon.power && Objects.equals(name, pokemon.name) && Objects.equals(type, pokemon.type);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, type, power);
        }

        @Override
        public String toString() {
            return String.format("%s(%d)", name, power);
        }

        @Override
        public int compareTo(Pokemon o) {
            if (name.compareTo(o.name) == 0) {
                return o.power - power;
            } else {
                return name.compareTo(o.name);
            }
        }
    }
}
