package JavaDSA.CodingTasks.MapSetHashTables.NoahTask;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Noah {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Map<String, Integer> animals = new HashMap<>();
        int n = Integer.parseInt(sc.nextLine());

        for (int i = 0; i < n; i++) {
            String input = sc.nextLine();
            if (!animals.containsKey(input)) {
                animals.put(input, 0);
            }
            animals.put(input, animals.get(input) + 1);
        }
        animals.keySet().stream().sorted()
                .forEach(animal ->
                        System.out.printf("%s %d %s%n",
                                animal,
                                animals.get(animal),
                                animals.get(animal) % 2 == 0 ? "Yes" : "No"));
    }
}
