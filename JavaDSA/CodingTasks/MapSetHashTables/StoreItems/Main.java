package JavaDSA.CodingTasks.MapSetHashTables.StoreItems;

import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static final String ADD_COMMAND = "add";
    public static final String FILTER_BY_TYPE_COMMAND = "filter by type";
    public static final int FILTER_LIMIT = 10;
    public static final String FILTER_BY_PRICE_FROM_COMMAND = "filter by price from";
    private static final String FILTER_BY_PRICE_TO_COMMAND = "filter by price to";
    public static Set<Item> items;
    public static Map<String, List<Item>> categories;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        items = new HashSet<>();
        categories = new HashMap<>();

        String input = sc.nextLine();

        while (!input.equals("end")) {

            if (input.startsWith(ADD_COMMAND)) {
                String[] inputArgs = input.split("\\s+");
                System.out.println(addItem(inputArgs[1], Double.parseDouble(inputArgs[2]), inputArgs[3]));
            } else if (input.startsWith(FILTER_BY_TYPE_COMMAND)) {
                String targetType = input.substring(FILTER_BY_TYPE_COMMAND.length() + 1);
                System.out.println(filterByType(targetType));
            } else if (input.startsWith(FILTER_BY_PRICE_FROM_COMMAND)) {
                input = input.substring(FILTER_BY_PRICE_FROM_COMMAND.length() + 1);
                String[] inputArgs = input.split(" to ");
                if (inputArgs.length == 2) {
                    System.out.println(filterByPriceFromTo(
                            Double.parseDouble(inputArgs[0]),
                            Double.parseDouble(inputArgs[1])));
                } else {
                    System.out.println(filterByPriceFrom(Double.parseDouble(input)));
                }
            } else if (input.startsWith(FILTER_BY_PRICE_TO_COMMAND)) {
                input = input.substring(FILTER_BY_PRICE_TO_COMMAND.length() + 1);

                System.out.println(filterByPriceTo(Double.parseDouble(input)));

            }

            input = sc.nextLine();
        }
    }

    private static String filterByPriceTo(double to) {
        StringBuilder builder = new StringBuilder("Ok: ");
        List<Item> sortedAndFiltered =
                items
                        .stream()
                        .sorted()
                        .filter(item -> item.price <= to)
                        .collect(Collectors.toList());
        for (int i = 0; i < Math.min(sortedAndFiltered.size(), FILTER_LIMIT); i++) {
            builder.append(String.format("%s(%.2f)", sortedAndFiltered.get(i).name, sortedAndFiltered.get(i).price));
            if (i != Math.min(sortedAndFiltered.size(), FILTER_LIMIT) - 1) {
                builder.append(", ");
            }
        }
        return builder.toString();
    }

    private static String filterByPriceFrom(double from) {
        StringBuilder builder = new StringBuilder("Ok: ");
        List<Item> sortedAndFiltered =
                items
                        .stream()
                        .sorted()
                        .filter(item -> item.price >= from)
                        .collect(Collectors.toList());
        for (int i = 0; i < Math.min(sortedAndFiltered.size(), FILTER_LIMIT); i++) {
            builder.append(String.format("%s(%.2f)", sortedAndFiltered.get(i).name, sortedAndFiltered.get(i).price));
            if (i != Math.min(sortedAndFiltered.size(), FILTER_LIMIT) - 1) {
                builder.append(", ");
            }
        }
        return builder.toString();
    }

    private static String filterByPriceFromTo(double from, double to) {
        StringBuilder builder = new StringBuilder("Ok: ");
        List<Item> sortedAndFiltered =
                items
                        .stream()
                        .sorted()
                        .filter(item -> item.price >= from && item.price <= to)
                        .collect(Collectors.toList());
        for (int i = 0; i < Math.min(sortedAndFiltered.size(), FILTER_LIMIT); i++) {
            builder.append(String.format("%s(%.2f)", sortedAndFiltered.get(i).name, sortedAndFiltered.get(i).price));
            if (i != Math.min(sortedAndFiltered.size(), FILTER_LIMIT) - 1) {
                builder.append(", ");
            }
        }
        return builder.toString();
    }

    private static String filterByType(String type) {

        if (!categories.containsKey(type)) {
            return String.format("Error: Type %s does not exist", type);
        }

        List<Item> sortedAndFiltered = categories.get(type).stream().sorted().collect(Collectors.toList());

        StringBuilder builder = new StringBuilder("Ok: ");
        for (int i = 0; i < Math.min(sortedAndFiltered.size(), FILTER_LIMIT); i++) {
            builder.append(String.format("%s(%.2f)", sortedAndFiltered.get(i).name, sortedAndFiltered.get(i).price));
            if (i != Math.min(sortedAndFiltered.size(), FILTER_LIMIT) - 1) {
                builder.append(", ");
            }
        }
        return builder.toString();
    }

    private static String addItem(String name, double price, String type) {
        Item itemToAdd = new Item(name, price, type);
        if (items.add(itemToAdd)) {
            if (!categories.containsKey(type)) {
                categories.put(type, new ArrayList<>());
            }
            categories.get(type).add(itemToAdd);
            return String.format("Ok: Item %s added successfully", name);
        }

        return String.format("Error: Item %s already exists", name);
    }

    public static class Item implements Comparable<Item> {
        private String name;
        private double price;
        private String type;

        public Item(String name, double price, String type) {
            this.name = name;
            this.price = price;
            this.type = type;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Item item = (Item) o;
            return Objects.equals(name, item.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name);
        }


        @Override
        public int compareTo(Item o) {
            if (Double.compare(this.price, o.price) == 0) {
                return this.name.compareTo(o.name);
            } else {
                return Double.compare(this.price, o.price);
            }
        }
    }
}