package JavaDSA.CodingTasks.MapSetHashTables.JustCount;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        Map<Character, Integer> symbols = new HashMap<>();
        Map<Character, Integer> upperCase = new HashMap<>();
        Map<Character, Integer> lowerCase = new HashMap<>();

        for (int i = 0; i < str.length(); i++) {
            char currentSymbol = str.charAt(i);
            if (Character.isUpperCase(currentSymbol)) {
                if (!upperCase.containsKey(currentSymbol)) {
                    upperCase.put(currentSymbol, 0);
                }
                upperCase.put(currentSymbol, upperCase.get(currentSymbol) + 1);
            } else if (Character.isLowerCase(currentSymbol)) {
                if (!lowerCase.containsKey(currentSymbol)) {
                    lowerCase.put(currentSymbol, 0);
                }
                lowerCase.put(currentSymbol, lowerCase.get(currentSymbol) + 1);
            } else {
                if (!symbols.containsKey(currentSymbol)) {
                    symbols.put(currentSymbol, 0);
                }
                symbols.put(currentSymbol, symbols.get(currentSymbol) + 1);
            }
        }
        Character symbolsBestChar = getMaxKey(symbols);
        Character lowerCaseBestChar = getMaxKey(lowerCase);
        Character upperCaseBestChar = getMaxKey(upperCase);

        System.out.println(symbolsBestChar == null ? '-' : symbolsBestChar + " " + symbols.get(symbolsBestChar));
        System.out.println(lowerCaseBestChar == null ? '-' : lowerCaseBestChar + " " + lowerCase.get(lowerCaseBestChar));
        System.out.println(upperCaseBestChar == null ? '-' : upperCaseBestChar + " " + upperCase.get(upperCaseBestChar));
    }

    private static Character getMaxKey(Map<Character, Integer> map) {
        int maxOccurrences = 0;
        Character bestKey = null;
        for (Character character : map.keySet()) {
            int currentSymbolOccurrences = map.get(character);
            if (currentSymbolOccurrences > maxOccurrences) {
                maxOccurrences = currentSymbolOccurrences;
                bestKey = character;
            } else if (currentSymbolOccurrences == maxOccurrences) {
                if ((int) bestKey > (int) character) {
                    bestKey = character;
                }
            }
        }
        return bestKey;
    }
}
