package com.company.dsa;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FilesUtils {
    private static int nestingLevel = 0;
    private static final List<String> files = new ArrayList<>();
    private static final Map<String, Integer> stats = new HashMap<>();

    public static void traverseDirectories(String path) {
        File file = new File(path);
        if (file.isDirectory()) {
            System.out.println("  ".repeat(nestingLevel++) + file.getName() + ":");
            File[] files = file.listFiles();
            for (File innerFile : files) {
                traverseDirectories(innerFile.getPath());
            }
            nestingLevel--;
        } else {
            System.out.println("  ".repeat(nestingLevel) + file.getName());
        }
    }

    public static List<String> findFiles(String path, String extension) {
        File file = new File(path);
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File innerFile : files) {
                findFiles(innerFile.getPath(), extension);
            }
        } else {
            if (file.getName().endsWith(extension)) {
                files.add(file.getName());
            }
        }
        return files;
    }

    public static boolean fileExists(String path, String fileName) {
        File file = new File(path);
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File innerFile : files) {
                if (fileExists(innerFile.getPath(), fileName)) {
                    return true;
                }
            }
        } else {
            return file.getName().equals(fileName);
        }
        return false;
    }

    public static Map<String, Integer> getDirectoryStats(String path) {
        File file = new File(path);
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File innerFile : files) {
                getDirectoryStats(innerFile.getPath());
            }
        } else {
            String extension = file.getName().split("\\.")[1];
            if (!stats.containsKey(extension)) {
                stats.put(extension, 0);
            }
            stats.put(extension, stats.get(extension) + 1);
        }
        return stats;
    }
}
