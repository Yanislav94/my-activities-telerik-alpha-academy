package JavaDSA.InClassActivity.Skeleton.src.com.company.dsa;

import java.nio.file.Paths;

import static com.company.dsa.FilesUtils.fileExists;
import static com.company.dsa.FilesUtils.findFiles;


public class Main {

    public static void main(String[] args) {
        String path = Paths.get("demo").toAbsolutePath().toString();
        System.out.println(com.company.dsa.FilesUtils.getDirectoryStats(path));
        System.out.println(fileExists(path, "test-3.md"));
        System.out.println(fileExists(path, "test-2.md"));
        com.company.dsa.FilesUtils.traverseDirectories(path);
        System.out.println(findFiles(path, "md"));
    }
}
