package JavaOOP.OOPInHomeActivityForumPost;

import java.util.ArrayList;

public class ForumPost {
    // Task 1:
    String author;
    String text;
    int upVotes;

    //
    ArrayList<String> replies;

    // Task 3:
    public ForumPost(String author, String text) {
        this(author, text, 0);
    }

    // Task 2:
    public ForumPost(String author, String text, int upVotes) {
        this.author = author;
        this.text = text;
        this.upVotes = upVotes;
        this.replies = new ArrayList<>();
    }

    // Task 4:
    public String basicFormat() {
        return String.format("%s / by %s, %d votes. %n", this.text, this.author, this.upVotes);
    }

    // Task 5:
    public String format() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.basicFormat());
        for (String reply : this.replies) {
            builder.append(String.format("- %s%n", reply));
        }
        builder.append(System.lineSeparator());
        return builder.toString();
    }

}
