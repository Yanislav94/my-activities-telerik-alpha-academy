package com.company.oop;

import java.util.Arrays;
import java.util.Iterator;

public class MyListImpl<T> implements MyList<T> {

    private static final int DEFAULT_CAPACITY = 4;
    private int capacity;
    private int size;
    private T[] array;

    public MyListImpl() {
        this.array = (T[]) new Object[DEFAULT_CAPACITY];
        this.capacity = DEFAULT_CAPACITY;
    }

    public MyListImpl(int capacity) {
        this.array = (T[]) new Object[capacity];
        this.capacity = capacity;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public int capacity() {
        return this.capacity;
    }

    @Override
    public void add(T element) {
        if (this.size >= this.capacity) {
            resizeArray();
        }
        this.array[size] = element;
        size++;
    }


    @Override
    public T get(int index) {
        if (indexIsOutOfBounds(index)) {
            throw new ArrayIndexOutOfBoundsException("Invalid index.");
        }
        return this.array[index];
    }

    @Override
    public int indexOf(T element) {
        for (int i = 0; i < size; i++) {
            if (this.array[i].equals(element)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(T element) {
        for (int i = size - 1; i >= 0; i--) {
            if (this.array[i].equals(element)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean contains(T element) {
        for (int i = 0; i < size; i++) {
            if (this.array[i].equals(element)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void removeAt(int index) {
        if (indexIsOutOfBounds(index)) {
            throw new ArrayIndexOutOfBoundsException("Invalid index.");
        }
        this.remove(this.get(index));
    }

    @Override
    public boolean remove(T element) {
        if (!this.contains(element)) {
            return false;
        }
        int index = this.indexOf(element);

        removeElementAndUpdateArray(index);
        return true;
    }

    @Override
    public void clear() {
        this.size = 0;
        this.array = (T[]) new Object[DEFAULT_CAPACITY];
        this.capacity = DEFAULT_CAPACITY;
    }

    @Override
    public void swap(int from, int to) {
        if (indexIsOutOfBounds(from) || indexIsOutOfBounds(to)) {
            throw new ArrayIndexOutOfBoundsException("Invalid index.");
        }

        T temp = this.get(from);
        this.array[from] = this.array[to];
        this.array[to] = temp;
    }

    @Override
    public void print() {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for (int i = 0; i < size; i++) {
            builder.append(this.get(i));
            if (i < size - 1) {
                builder.append(", ");
            }
        }
        builder.append("]");
        System.out.println(builder);
    }

    @Override
    public Iterator<T> iterator() {
        return new MyListImplIterator();
    }

    private void resizeArray() {
        this.capacity *= 2;
        this.array = Arrays.copyOf(this.array, this.capacity);
    }

    private void removeElementAndUpdateArray(int index) {
        T[] newArray = (T[]) new Object[this.capacity];
        int currentIndex = 0;

        System.arraycopy(this.array, 0, newArray, 0, index);
        System.arraycopy(this.array, index + 1, newArray, index + 1, size - (index + 1));
        this.size--;
        this.array = newArray;
    }

    private boolean indexIsOutOfBounds(int index) {
        return index >= this.size || index < 0;
    }

    private class MyListImplIterator implements Iterator<T> {

        private int currentPosition;

        public MyListImplIterator() {
            this.currentPosition = 0;
        }

        @Override
        public boolean hasNext() {
            return currentPosition < size;
        }

        @Override
        public T next() {
            return array[currentPosition++];
        }
    }
}
