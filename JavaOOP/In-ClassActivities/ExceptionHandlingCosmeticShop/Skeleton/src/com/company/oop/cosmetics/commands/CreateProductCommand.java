package com.company.oop.cosmetics.commands;

import com.company.oop.cosmetics.commands.contracts.Command;
import com.company.oop.cosmetics.core.contracts.ProductRepository;
import com.company.oop.cosmetics.exceptions.ExistingEntityException;
import com.company.oop.cosmetics.models.GenderType;
import com.company.oop.cosmetics.utils.ParsingHelpers;
import com.company.oop.cosmetics.utils.ValidationHelpers;

import java.util.List;

public class CreateProductCommand implements Command {

    private static final int PARAMETERS_COUNT = 4;

    private static final String PRODUCT_CREATED = "Product with name %s was created!";

    private final ProductRepository productRepository;

    public CreateProductCommand(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters
                , PARAMETERS_COUNT
                , "CreateProduct");

        String name = parameters.get(0);
        String brand = parameters.get(1);
        double price = ParsingHelpers.doubleTryParse(
                parameters.get(2)
                , "price"
                , "Third");

        GenderType gender = ParsingHelpers.tryParseEnum(
                parameters.get(3).toUpperCase()
                , GenderType.class
                , "Forth");

        return createProduct(name, brand, price, gender);
    }

    private String createProduct(String name, String brand, double price, GenderType gender) {
        if (productRepository.productExist(name)) {
            throw new ExistingEntityException(String.format("Product %s already exist.", name));
        }

        productRepository.createProduct(name, brand, price, gender);

        return String.format(PRODUCT_CREATED, name);
    }

}
