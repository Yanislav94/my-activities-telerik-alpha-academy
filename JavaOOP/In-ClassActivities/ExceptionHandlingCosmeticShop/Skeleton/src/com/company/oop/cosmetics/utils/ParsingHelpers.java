package com.company.oop.cosmetics.utils;

import com.company.oop.cosmetics.exceptions.DoubleParseException;
import com.company.oop.cosmetics.exceptions.InvalidEnumException;

public class ParsingHelpers {
    public static double doubleTryParse(String valueToParse, String parameterName, String parameterPosition) {
        try {
            return Double.parseDouble(valueToParse);
        } catch (NumberFormatException e) {
            throw new DoubleParseException(
                    String.format("%s parameter should be %s (real number)."
                            , parameterPosition
                            , parameterName));
        }
    }

    public static <E extends Enum<E>> E tryParseEnum(String valueToParse, Class<E> type, String parameterPosition) {
        try {
            return Enum.valueOf(type, valueToParse.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new InvalidEnumException(
                    String.format("%s parameter should be one of %s."
                            , parameterPosition
                            , getOrdinalsWithComaSeparator(type.getEnumConstants())));
        }
    }

    private static <E extends Enum<E>> String getOrdinalsWithComaSeparator(E[] enumConstants) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < enumConstants.length; i++) {
            builder.append(enumConstants[i].toString());
            if (i < enumConstants.length - 2) {
                builder.append(", ");
            } else if (i < enumConstants.length - 1) {
                builder.append(" or ");
            }
        }
        return builder.toString();
    }

}
