package com.company.oop.cosmetics.utils;


import com.company.oop.cosmetics.exceptions.InvalidArgumentsCount;
import com.company.oop.cosmetics.exceptions.InvalidNameException;

import java.util.List;

public class ValidationHelpers {

    private static final String INVALID_NUMBER_OF_ARGUMENTS = "%s command expects %d parameters.";

    public static void validateStringLength(String name, int minNameLength, int maxNameLength, String message) {
        validateValueInRange(name.length(), minNameLength, maxNameLength, message);
    }

    public static void validateValueInRange(double value, double min, double max, String errorMessage) {
        if (value < min || value > max) {
            throw new InvalidNameException(errorMessage);
        }
    }

    public static void validateArgumentsCount(List<String> list, int expectedParametersCount, String commandName) {
        if (list.size() < expectedParametersCount || list.size() > expectedParametersCount) {
            throw new InvalidArgumentsCount(String.format(INVALID_NUMBER_OF_ARGUMENTS, commandName, expectedParametersCount));
        }
    }
}
