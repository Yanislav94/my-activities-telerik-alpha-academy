package com.company.oop.cosmetics.exceptions;

public class InvalidCommandException extends RuntimeException{

    public InvalidCommandException(String message) {
        super(message);
    }
}
