package com.company.oop.cosmetics.exceptions;

public class DoubleParseException extends RuntimeException {
    public DoubleParseException(String message) {
        super(message);
    }
}
