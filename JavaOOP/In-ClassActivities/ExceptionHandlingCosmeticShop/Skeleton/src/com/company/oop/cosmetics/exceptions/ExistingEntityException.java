package com.company.oop.cosmetics.exceptions;

public class ExistingEntityException extends RuntimeException {
    public ExistingEntityException(String message) {
        super(message);
    }
}
