package com.company.oop.cosmetics.exceptions;

public class NegativePriceException extends RuntimeException{


    public NegativePriceException(String message) {
        super(message);
    }
}
