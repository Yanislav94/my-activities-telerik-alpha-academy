package com.company.oop.cosmetics.exceptions;

public class InvalidArgumentsCount extends RuntimeException {


    public InvalidArgumentsCount(String message) {
        super(message);
    }
}
