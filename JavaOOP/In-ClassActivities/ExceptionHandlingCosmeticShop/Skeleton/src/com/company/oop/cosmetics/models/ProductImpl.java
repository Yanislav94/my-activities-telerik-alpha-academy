package com.company.oop.cosmetics.models;

import com.company.oop.cosmetics.exceptions.NegativePriceException;
import com.company.oop.cosmetics.models.contracts.Product;
import com.company.oop.cosmetics.utils.ValidationHelpers;

public class ProductImpl implements Product {

    private static final int MIN_NAME_LENGTH = 3;
    private static final int MAX_NAME_LENGTH = 10;
    private static final int MIN_BRAND_NAME_LENGTH = 2;
    private static final int MAX_BRAND_NAME_LENGTH = 10;

    private String name;
    private String brand;
    private double price;
    private final GenderType gender;

    public ProductImpl(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        ValidationHelpers.validateStringLength(name
                , MIN_NAME_LENGTH
                , MAX_NAME_LENGTH
                , String.format("Product name should be between %d and %d symbols."
                        , MIN_NAME_LENGTH
                        , MAX_NAME_LENGTH));
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    private void setBrand(String brand) {
        ValidationHelpers.validateStringLength(brand
                , MIN_BRAND_NAME_LENGTH
                , MAX_BRAND_NAME_LENGTH
                , String.format("Product brand should be between %d and %d symbols."
                        , MIN_BRAND_NAME_LENGTH
                        , MAX_BRAND_NAME_LENGTH));
        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        if (price < 0) {
            throw new NegativePriceException("Price can't be negative.");
        }
        this.price = price;
    }

    public GenderType getGender() {
        return gender;
    }

    @Override
    public String print() {
        return String.format(
                "#%s %s%n" +
                        " #Price: $%.2f%n" +
                        " #Gender: %s%n",
                name,
                brand,
                price,
                gender);
    }

}
