package com.company.oop.cosmetics.exceptions;

public class InvalidNameException extends RuntimeException{
    public InvalidNameException(String message)
    {
        super(message);
    }
}
