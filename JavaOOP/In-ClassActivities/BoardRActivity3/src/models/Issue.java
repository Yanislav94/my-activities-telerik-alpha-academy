package models;

import java.time.LocalDate;

public class Issue extends BoardItem {
    private String description;

    public Issue(String title, String description, LocalDate dueDate) {
        super(title, dueDate);
        this.setDescription(description);
    }

    private void setDescription(String description) {
        if (description.trim().isEmpty()) {
            this.description = "No description";
            return;
        }
        this.description = description;
    }
}
