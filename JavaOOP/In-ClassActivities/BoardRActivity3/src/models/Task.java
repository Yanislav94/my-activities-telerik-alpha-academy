package models;

import enums.Status;

import java.time.LocalDate;

public class Task extends BoardItem {
    private String assignee;

    public Task(String title, String assignee, LocalDate dueDate) {
        super(title, dueDate);
        this.setAssignee(assignee);
        super.setStatus(Status.ToDo);
    }

    public String getAssignee() {
        return assignee;
    }


    void setAssignee(String assignee) {
        if (assignee.trim().isEmpty() || assignee.length() < 5 || assignee.length() > 30) {
            throw new IllegalArgumentException("Invalid assignee!");
        }
        if (this.assignee != null) {
            eventLogs.add(new EventLog(String.format("Title changed from %s to %s", this.assignee, assignee)));
        }
        this.assignee = assignee;
    }
}
