import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class BoardItem {

    // TASK 1:
    private String title;
    private LocalDate dueDate;
    private Status status;

    private List<EventLog> eventLogs;

    public BoardItem(String title, LocalDate dueDate) {
        this.setTitle(title);
        this.setDueDate(dueDate);
        this.setStatus(Status.Open);
        this.eventLogs = new ArrayList<>();
        eventLogs.add(new EventLog(String.format("Item created: %s", this.viewInfo())));
    }

    public void setTitle(String title) {
        if (title.length() < 5 || title.length() > 30) {
            throw new IllegalArgumentException("Please provide a title with length between 5 and 30 chars");
        }
        if (this.title != null) {
            eventLogs.add(new EventLog(String.format("Title changed from %s to %s", this.title, title)));
        }
        this.title = title;
    }

    public void setDueDate(LocalDate dueDate) {
        if (dueDate.isBefore(LocalDate.now())) {
            throw new IllegalArgumentException("Please provide a dueDate that is not in the past");
        }
        if (this.dueDate != null) {
            eventLogs.add(new EventLog(String.format("DueDate changed from %s to %s", this.dueDate, dueDate)));
        }
        this.dueDate = dueDate;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return this.status;
    }

    public String getTitle() {
        return this.title;
    }

    public LocalDate getDueDate() {
        return this.dueDate;
    }

    // TASK 2:
    public void advanceStatus() {
        if (this.status == Status.Verified) {
            eventLogs.add(new EventLog("Can't advance, already at Verified"));
            return;
        }
        Status[] statuses = Status.values();
        for (int i = 0; i < statuses.length; i++) {
            if (statuses[i] == this.status && i < statuses.length - 1) {
                Status newStatus = statuses[i + 1];
                eventLogs.add(new EventLog(String.format("Status changed from %s to %s", this.status.displayName(), newStatus.displayName())));
                this.status = newStatus;
                break;
            }
        }
    }

    // TASK 2:
    public void revertStatus() {
        if (this.status == Status.Open) {
            eventLogs.add(new EventLog("Can't revert, already at Open"));
            return;
        }
        Status[] statuses = Status.values();
        for (int i = 0; i < statuses.length; i++) {
            if (statuses[i] == this.status && i > 0) {
                Status newStatus = statuses[i - 1];
                eventLogs.add(new EventLog(String.format("Status changed from %s to %s", this.status.displayName(), newStatus.displayName())));
                this.status = newStatus;
                break;
            }
        }
    }

    public String viewInfo() {
        return String.format("'%s', [%s | %s]", this.title, this.status, this.dueDate);
    }

    public void displayHistory() {
        StringBuilder builder = new StringBuilder();

        for (EventLog eventLog : this.eventLogs) {
            builder.append(eventLog.viewInfo()).append(System.lineSeparator());
        }
        System.out.println(builder.toString().trim());
    }
}
