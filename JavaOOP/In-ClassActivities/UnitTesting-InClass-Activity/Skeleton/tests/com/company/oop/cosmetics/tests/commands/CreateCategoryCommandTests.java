package com.company.oop.cosmetics.tests.commands;

import com.company.oop.cosmetics.commands.CreateCategoryCommand;
import com.company.oop.cosmetics.commands.contracts.Command;
import com.company.oop.cosmetics.core.ProductRepositoryImpl;
import com.company.oop.cosmetics.core.contracts.ProductRepository;
import com.company.oop.cosmetics.exceptions.DuplicateEntityException;
import com.company.oop.cosmetics.exceptions.InvalidUserInputException;
import com.company.oop.cosmetics.models.CategoryImpl;
import com.company.oop.cosmetics.models.contracts.Category;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateCategoryCommandTests {
    public static final String CATEGORY_NAME = "Name";
    private ProductRepository repository;
    private Command command;
    private Category category;


    @BeforeEach
    public void SetUp() {
        this.repository = new ProductRepositoryImpl();
        this.command = new CreateCategoryCommand(repository);
        this.category = new CategoryImpl(CATEGORY_NAME);
    }

    @Test
    public void execute_Should_AddNewCategoryToRepository_When_ValidParameters() {
        List<String> parameters = new ArrayList<>();
        parameters.add(CATEGORY_NAME);

        this.command.execute(parameters);
        Category category = this.repository.findCategoryByName(CATEGORY_NAME);
        Assertions.assertEquals(CATEGORY_NAME, category.getName());
    }

    @Test
    public void execute_Should_ThrowException_When_MissingParameters() {
        List<String> parameters = new ArrayList<>();
        Assertions.assertThrows(InvalidUserInputException.class, () -> this.command.execute(parameters));
    }

    @Test
    public void execute_Should_ThrowException_When_DuplicateCategoryName() {
        List<String> parameters = new ArrayList<>();
        parameters.add(CATEGORY_NAME);
        this.command.execute(parameters);
        Assertions.assertThrows(DuplicateEntityException.class, () -> this.command.execute(parameters));
    }
}
