package com.company.oop.cosmetics.tests.commands;

import com.company.oop.cosmetics.commands.AddProductToCategoryCommand;
import com.company.oop.cosmetics.commands.CreateCategoryCommand;
import com.company.oop.cosmetics.commands.CreateProductCommand;
import com.company.oop.cosmetics.commands.contracts.Command;
import com.company.oop.cosmetics.core.ProductRepositoryImpl;
import com.company.oop.cosmetics.core.contracts.ProductRepository;
import com.company.oop.cosmetics.exceptions.InvalidUserInputException;
import com.company.oop.cosmetics.models.CategoryImpl;
import com.company.oop.cosmetics.models.GenderType;
import com.company.oop.cosmetics.models.ProductImpl;
import com.company.oop.cosmetics.models.contracts.Category;
import com.company.oop.cosmetics.models.contracts.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class AddProductToCategoryCommandTests {

    public static final String CATEGORY_NAME = "Name";
    public static final String PRODUCT_NAME = "Name";
    public static final String PRODUCT_BRAND = "Brand";
    public static final double PRODUCT_PRICE = 2.5;
    public static final GenderType PRODUCT_GENDER = GenderType.UNISEX;
    private ProductRepository repository;
    private Command command;
    private Product product;
    private Category category;
    private List<String> parameters;

    @BeforeEach
    public void SetUp() {
        this.repository = new ProductRepositoryImpl();
        this.command = new AddProductToCategoryCommand(repository);
        this.product = new ProductImpl(PRODUCT_NAME, PRODUCT_BRAND, PRODUCT_PRICE, PRODUCT_GENDER);
        this.category = new CategoryImpl(CATEGORY_NAME);
        this.parameters = new ArrayList<>();
        parameters.add(CATEGORY_NAME);
        parameters.add(PRODUCT_NAME);
    }

    @Test
    public void execute_Should_AddNewProductToCategory_When_ValidParameters() {
        CreateCategoryCommand createCategoryCommand = new CreateCategoryCommand(this.repository);
        List<String> createCategoryParams = new ArrayList<>();
        createCategoryParams.add(CATEGORY_NAME);
        createCategoryCommand.execute(createCategoryParams);

        CreateProductCommand createProductCommand = new CreateProductCommand(this.repository);
        List<String> createProductParams = new ArrayList<>();
        createProductParams.add(PRODUCT_NAME);
        createProductParams.add(PRODUCT_BRAND);
        createProductParams.add(String.valueOf(PRODUCT_PRICE));
        createProductParams.add(String.valueOf(PRODUCT_GENDER));
        createProductCommand.execute(createProductParams);

        this.command.execute(parameters);
        this.command.execute(this.parameters);
        Assertions.assertEquals(PRODUCT_NAME, this.repository.findCategoryByName(CATEGORY_NAME).getProducts().get(0).getName());
    }

    @Test
    public void execute_Should_ThrowException_When_MissingParameters() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> this.command.execute(new ArrayList<>()));
    }
}
