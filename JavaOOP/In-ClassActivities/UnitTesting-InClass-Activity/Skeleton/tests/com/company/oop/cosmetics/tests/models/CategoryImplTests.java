package com.company.oop.cosmetics.tests.models;

import com.company.oop.cosmetics.exceptions.InvalidUserInputException;
import com.company.oop.cosmetics.models.CategoryImpl;
import com.company.oop.cosmetics.models.GenderType;
import com.company.oop.cosmetics.models.ProductImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CategoryImplTests {

    public static final int NAME_MIN_LENGTH = 3;
    public static final int NAME_MAX_LENGTH = 10;
    public static final String PRODUCT_NAME = "Name";
    public static final String PRODUCT_BRAND = "Brand";
    public static final double PRODUCT_PRICE = 2.5;
    public static final GenderType PRODUCT_GENDER = GenderType.UNISEX;

    private static final String VALID_NAME = "x".repeat(NAME_MIN_LENGTH);
    private static final String INVALID_NAME_SHORTER = "x".repeat(NAME_MIN_LENGTH - 1);
    private static final String INVALID_NAME_LONGER = "x".repeat(NAME_MAX_LENGTH + 1);

    private static final String EMPTY_PRODUCTS_PRINT_OUTPUT =
            String.format("#Category: %s%n" +
                    " #No product in this category", VALID_NAME);

    private static final String PRODUCTS_PRESENT_PRINT_OUTPUT =
            String.format("#Category: %s%n" +
                            "#%s %s%n" +
                            " #Price: $%.2f%n" +
                            " #Gender: %s%n" +
                            " ===%n"
                    , VALID_NAME
                    , PRODUCT_NAME
                    , PRODUCT_BRAND
                    , PRODUCT_PRICE
                    , PRODUCT_GENDER);
    private CategoryImpl category;
    private ProductImpl product;

    @BeforeEach
    public void SetUp() {
        this.category = new CategoryImpl(VALID_NAME);
        this.product = new ProductImpl(PRODUCT_NAME, PRODUCT_BRAND, PRODUCT_PRICE, PRODUCT_GENDER);
    }


    @Test
    public void constructor_Should_InitializeName_When_ArgumentsAreValid() {
        Assertions.assertEquals(VALID_NAME, this.category.getName());
    }

    @Test
    public void constructor_Should_InitializeProducts_When_ArgumentsAreValid() {
        Assertions.assertNotNull(this.category.getProducts());
    }

    @Test
    public void constructor_Should_ThrowException_When_NameIsShorterThanExpected() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> new CategoryImpl(INVALID_NAME_SHORTER));
    }

    @Test
    public void addProduct_Should_AddProductToList() {
        Assertions.assertEquals(0, this.category.getProducts().size());
        this.category.addProduct(product);
        Assertions.assertEquals(1, this.category.getProducts().size());
    }

    @Test
    public void removeProduct_Should_RemoveProductFromList_When_ProductExist() {
        this.category.addProduct(product);
        Assertions.assertEquals(1, this.category.getProducts().size());
        this.category.removeProduct(product);
        Assertions.assertEquals(0, this.category.getProducts().size());
    }

    @Test
    public void removeProduct_should_notRemoveProductFromList_when_productNotExist() {
        this.category.addProduct(product);
        this.category.removeProduct(new ProductImpl("OtherName", "OtherBrand", 1, GenderType.UNISEX));
        Assertions.assertEquals(1, this.category.getProducts().size());
    }

    //Adding print() method validation in order to have 100% coverage
    @Test
    public void print_should_PrintNoProducts_When_NoProducts() {
        String output = this.category.print();
        Assertions.assertEquals(EMPTY_PRODUCTS_PRINT_OUTPUT, output);
    }

    @Test
    public void print_should_PrintAllProducts_When_ProductsInTheList() {
        this.category.addProduct(this.product);
        String output = this.category.print();
        Assertions.assertEquals(PRODUCTS_PRESENT_PRINT_OUTPUT, output);
    }

}
