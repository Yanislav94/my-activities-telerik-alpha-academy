package com.company.oop.cosmetics.tests.models;

import com.company.oop.cosmetics.exceptions.InvalidUserInputException;
import com.company.oop.cosmetics.models.GenderType;
import com.company.oop.cosmetics.models.ProductImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ProductImplTests {
    private static final int VALID_PRODUCT_NAME_LENGTH = 10;
    private static final int INVALID_PRODUCT_NAME_LENGTH = VALID_PRODUCT_NAME_LENGTH + 1;
    private static final String INVALID_NAME_MESSAGE = String.format(
            "Product name should be between %d and %d symbols.",
            ProductImpl.NAME_MIN_LENGTH,
            ProductImpl.NAME_MAX_LENGTH);
    private static final int VALID_PRODUCT_BRAND_LENGTH = 10;
    private static final int INVALID_PRODUCT_BRAND_LENGTH = VALID_PRODUCT_BRAND_LENGTH + 1;

    private static final String INVALID_BRAND_MESSAGE = String.format(
            "Product brand should be between %d and %d symbols.",
            ProductImpl.BRAND_MIN_LENGTH,
            ProductImpl.BRAND_MAX_LENGTH);
    private static final double VALID_PRICE = 0;
    private static final double INVALID_PRICE = VALID_PRICE - 1;

    private static final String INVALID_PRICE_MESSAGE = "Price can't be negative.";

    private ProductImpl product;

    @BeforeEach
    public void SetUp() {
        product = new ProductImpl(
                "x".repeat(VALID_PRODUCT_NAME_LENGTH)
                , "x".repeat(VALID_PRODUCT_BRAND_LENGTH)
                , VALID_PRICE
                , GenderType.MEN);
    }

    @Test
    public void constructor_Should_ThrowException_When_NameIsInvalid() {
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> new ProductImpl(
                        "x".repeat(INVALID_PRODUCT_NAME_LENGTH)
                        , "x".repeat(VALID_PRODUCT_BRAND_LENGTH)
                        , VALID_PRICE
                        , GenderType.MEN));
    }

    @Test
    public void constructor_Should_ThrowExceptionWithMessage_When_NameIsInvalid() {
        ProductImpl invalidProduct = null;
        try {
            invalidProduct = new ProductImpl(
                    "x".repeat(INVALID_PRODUCT_NAME_LENGTH)
                    , "x".repeat(VALID_PRODUCT_BRAND_LENGTH)
                    , VALID_PRICE
                    , GenderType.MEN);
        } catch (InvalidUserInputException e) {
            Assertions.assertEquals(INVALID_NAME_MESSAGE, e.getMessage());
        }
        Assertions.assertNull(invalidProduct);
    }

    @Test
    public void constructor_Should_ThrowException_When_BrandIsInvalid() {
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> new ProductImpl(
                        "x".repeat(VALID_PRODUCT_NAME_LENGTH)
                        , "x".repeat(INVALID_PRODUCT_BRAND_LENGTH)
                        , VALID_PRICE
                        , GenderType.MEN));
    }

    @Test
    public void constructor_Should_ThrowExceptionWithMessage_When_BrandIsInvalid() {
        ProductImpl invalidProduct = null;
        try {
            invalidProduct = new ProductImpl(
                    "x".repeat(VALID_PRODUCT_NAME_LENGTH)
                    , "x".repeat(INVALID_PRODUCT_BRAND_LENGTH)
                    , VALID_PRICE
                    , GenderType.MEN);
        } catch (InvalidUserInputException e) {
            Assertions.assertEquals(INVALID_BRAND_MESSAGE, e.getMessage());
        }
        Assertions.assertNull(invalidProduct);
    }

    @Test
    public void constructor_Should_ThrowException_When_PriceIsInvalid() {
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> new ProductImpl(
                        "x".repeat(VALID_PRODUCT_NAME_LENGTH)
                        , "x".repeat(VALID_PRODUCT_BRAND_LENGTH)
                        , INVALID_PRICE
                        , GenderType.MEN));
    }

    @Test
    public void constructor_Should_ThrowExceptionWithMessage_When_PriceIsInvalid() {
        ProductImpl invalidProduct = null;
        try {
            invalidProduct = new ProductImpl(
                    "x".repeat(VALID_PRODUCT_NAME_LENGTH)
                    , "x".repeat(VALID_PRODUCT_BRAND_LENGTH)
                    , INVALID_PRICE
                    , GenderType.MEN);
        } catch (InvalidUserInputException e) {
            Assertions.assertEquals(INVALID_PRICE_MESSAGE, e.getMessage());
        }
        Assertions.assertNull(invalidProduct);
    }

    @Test
    public void constructor_Should_CreateAValidInstance_When_PropertiesAreValid() {
        Assertions.assertNotNull(product);
        Assertions.assertEquals("x".repeat(VALID_PRODUCT_NAME_LENGTH), product.getName());
        Assertions.assertEquals("x".repeat(VALID_PRODUCT_BRAND_LENGTH), product.getBrand());
        Assertions.assertEquals(VALID_PRICE, product.getPrice());
        Assertions.assertEquals(GenderType.MEN, product.getGender());

    }

    @Test
    public void print_Should_ReturnCorrectFormat_When_Called() {
        String correctOutput = String.format("#%s %s%n" +
                        " #Price: $%.2f%n" +
                        " #Gender: %s%n"
                , product.getName()
                , product.getBrand()
                , product.getPrice()
                , product.getGender());


        Assertions.assertEquals(correctOutput, product.print());
    }
}
