package com.company.oop.cosmetics.tests.commands;

import com.company.oop.cosmetics.commands.CreateProductCommand;
import com.company.oop.cosmetics.commands.contracts.Command;
import com.company.oop.cosmetics.core.ProductRepositoryImpl;
import com.company.oop.cosmetics.core.contracts.ProductRepository;
import com.company.oop.cosmetics.exceptions.DuplicateEntityException;
import com.company.oop.cosmetics.exceptions.InvalidUserInputException;
import com.company.oop.cosmetics.models.GenderType;
import com.company.oop.cosmetics.models.ProductImpl;
import com.company.oop.cosmetics.models.contracts.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateProductCommandTests {
    public static final String PRODUCT_NAME = "Name";
    public static final String PRODUCT_BRAND = "Brand";
    public static final double PRODUCT_PRICE = 2.5;
    public static final GenderType PRODUCT_GENDER = GenderType.UNISEX;

    private Product product;
    private ProductRepository repository;
    private Command command;
    private List<String> parameters;

    @BeforeEach
    public void SetUp() {
        this.product = new ProductImpl(PRODUCT_NAME, PRODUCT_BRAND, PRODUCT_PRICE, PRODUCT_GENDER);
        this.repository = new ProductRepositoryImpl();
        this.command = new CreateProductCommand(repository);
        this.parameters = new ArrayList<>();
        this.parameters.add(PRODUCT_NAME);
        this.parameters.add(PRODUCT_BRAND);
        this.parameters.add(String.valueOf(PRODUCT_PRICE));
        this.parameters.add(String.valueOf(PRODUCT_GENDER));
    }

    @Test
    public void execute_Should_AddNewProductToRepository_When_ValidParameters() {
        Assertions.assertEquals(0, this.repository.getProducts().size());
        this.command.execute(parameters);
        Assertions.assertEquals(1, this.repository.getProducts().size());
    }

    @Test
    public void execute_Should_ThrowException_When_MissingParameters() {
        this.parameters = new ArrayList<>();
        Assertions.assertThrows(InvalidUserInputException.class, () -> this.command.execute(parameters));
    }

    @Test
    public void execute_Should_ThrowException_When_DuplicateProductName() {
        this.command.execute(parameters);
        Assertions.assertThrows(DuplicateEntityException.class, () -> this.command.execute(parameters));
    }

    @Test
    public void execute_Should_ThrowException_When_PriceIsNotParsableToDouble() {
        //Adding name as Price in order to test the ParseDouble validation
        this.parameters.set(2, PRODUCT_NAME);
        Assertions.assertThrows(InvalidUserInputException.class, () -> this.command.execute(parameters));
    }
    @Test
    public void execute_Should_ThrowException_When_GenderIsNotParsable() {
        //Adding name as Gender in order to test the ParseGender validation
        this.parameters.set(3, PRODUCT_NAME);
        Assertions.assertThrows(InvalidUserInputException.class, () -> this.command.execute(parameters));
    }
}
