package com.company.oop.cosmetics.tests.commands;

import com.company.oop.cosmetics.commands.CreateCategoryCommand;
import com.company.oop.cosmetics.commands.ShowCategoryCommand;
import com.company.oop.cosmetics.commands.contracts.Command;
import com.company.oop.cosmetics.core.ProductRepositoryImpl;
import com.company.oop.cosmetics.core.contracts.ProductRepository;
import com.company.oop.cosmetics.exceptions.InvalidUserInputException;
import com.company.oop.cosmetics.models.GenderType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowCategoryCommandTests {
    private static final String VALID_NAME = "Name";

    private static final String EMPTY_PRODUCTS_PRINT_OUTPUT =
            String.format("#Category: %s%n" +
                    " #No product in this category", VALID_NAME);

    private ProductRepository repository;
    private Command command;
    List<String> parameters;

    @BeforeEach
    public void SetUp() {
        this.repository = new ProductRepositoryImpl();
        this.command = new ShowCategoryCommand(repository);
        this.parameters = new ArrayList<>();
        this.parameters.add(VALID_NAME);
    }

    @Test
    public void execute_Should_ShowCategories_When_ValidParameters() {
        Command addCategoryCommand = new CreateCategoryCommand(this.repository);
        addCategoryCommand.execute(this.parameters);
        Assertions.assertEquals(EMPTY_PRODUCTS_PRINT_OUTPUT, this.command.execute(this.parameters));
    }
    @Test
    public void execute_Should_ThrowException_When_MissingParameters() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> this.command.execute(new ArrayList<>()));
    }
}
