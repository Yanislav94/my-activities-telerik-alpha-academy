package com.company.oop.cosmetics.tests.core;

import com.company.oop.cosmetics.commands.CreateCategoryCommand;
import com.company.oop.cosmetics.commands.CreateProductCommand;
import com.company.oop.cosmetics.commands.contracts.Command;
import com.company.oop.cosmetics.core.ProductRepositoryImpl;
import com.company.oop.cosmetics.core.contracts.ProductRepository;
import com.company.oop.cosmetics.exceptions.InvalidUserInputException;
import com.company.oop.cosmetics.models.CategoryImpl;
import com.company.oop.cosmetics.models.GenderType;
import com.company.oop.cosmetics.models.ProductImpl;
import com.company.oop.cosmetics.models.contracts.Category;
import com.company.oop.cosmetics.models.contracts.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ProductRepositoryImplTests {
    public static final String VALID_NAME = "Name";
    public static final String PRODUCT_BRAND = "Brand";
    public static final double PRODUCT_PRICE = 2.5;
    public static final GenderType PRODUCT_GENDER = GenderType.UNISEX;

    private ProductRepository repository;
    private Product product;
    private Category category;

    @BeforeEach
    public void SetUp() {
        this.repository = new ProductRepositoryImpl();
        this.product = new ProductImpl(VALID_NAME, PRODUCT_BRAND, PRODUCT_PRICE, PRODUCT_GENDER);
        this.category = new CategoryImpl(VALID_NAME);
    }

    @Test
    public void constructor_Should_InitializeProducts() {
        Assertions.assertNotNull(this.repository.getProducts());
    }

    @Test
    public void constructor_Should_InitializeCategories() {
        Assertions.assertNotNull(this.repository.getCategories());
    }

    @Test
    public void getCategories_Should_ReturnCopyOfCollection() {
        Assertions.assertEquals(0, this.repository.getCategories().size());
        this.repository.getCategories().add(this.category);
        Assertions.assertEquals(0, this.repository.getProducts().size());
    }

    @Test
    public void getProducts_Should_ReturnCopyOfCollection() {
        Assertions.assertEquals(0, this.repository.getProducts().size());
        this.repository.getProducts().add(this.product);
        Assertions.assertEquals(0, this.repository.getProducts().size());
    }

    @Test
    public void categoryExists_Should_ReturnTrue_When_CategoryExists() {
        Command addCategory = new CreateCategoryCommand(this.repository);
        List<String> parameters = new ArrayList<>();
        parameters.add(VALID_NAME);
        addCategory.execute(parameters);
        Assertions.assertTrue(this.repository.categoryExist(VALID_NAME));
    }

    @Test
    public void categoryExists_Should_ReturnFalse_When_CategoryDoesNotExist() {
        Assertions.assertFalse(this.repository.categoryExist(VALID_NAME));
    }

    @Test
    public void productExists_Should_ReturnTrue_When_ProductExists() {
        addProductToRepository();
        Assertions.assertTrue(this.repository.productExist(VALID_NAME));
    }

    @Test
    public void productExists_Should_ReturnFalse_When_ProductDoesNotExist() {
        Assertions.assertFalse(this.repository.productExist(VALID_NAME));
    }

    @Test
    public void createProduct_Should_AddToProducts_When_ArgumentsAreValid() {
        Assertions.assertEquals(0, this.repository.getProducts().size());

        addProductToRepository();

        Assertions.assertEquals(1, this.repository.getProducts().size());
    }


    @Test
    public void createCategory_Should_AddToCategories_When_ArgumentsAreValid() {
        Assertions.assertEquals(0, this.repository.getCategories().size());

        addCategoryToRepository();

        Assertions.assertEquals(1, this.repository.getCategories().size());
    }

    @Test
    public void findCategoryByName_Should_ReturnCategory_When_Exists() {
        addCategoryToRepository();

        Assertions.assertNotNull(this.repository.findCategoryByName(VALID_NAME));
    }


    @Test
    public void findCategoryByName_Should_ThrowException_When_DoesNotExist() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> this.repository.findCategoryByName(VALID_NAME));
    }

    @Test
    public void findProductByName_Should_ReturnCategory_When_Exists() {
        addProductToRepository();

        Assertions.assertNotNull(this.repository.findProductByName(VALID_NAME));
        Assertions.assertEquals(VALID_NAME, this.repository.getProducts().get(0).getName());
    }

    @Test
    public void findProductByName_Should_ThrowException_When_DoesNotExist() {
        Assertions.assertThrows(InvalidUserInputException.class, () -> this.repository.findProductByName(VALID_NAME));
    }

    private void addCategoryToRepository() {
        Command addCategory = new CreateCategoryCommand(this.repository);
        List<String> parameters = new ArrayList<>();
        parameters.add(VALID_NAME);
        addCategory.execute(parameters);
    }

    private void addProductToRepository() {
        Command addProduct = new CreateProductCommand(this.repository);
        List<String> parameters = new ArrayList<>();
        parameters.add(VALID_NAME);
        parameters.add(PRODUCT_BRAND);
        parameters.add(String.valueOf(PRODUCT_PRICE));
        parameters.add(String.valueOf(PRODUCT_GENDER));
        addProduct.execute(parameters);
    }
}
