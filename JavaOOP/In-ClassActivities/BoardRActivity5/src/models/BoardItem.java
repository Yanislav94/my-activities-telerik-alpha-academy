package models;

import enums.Status;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public abstract class BoardItem {

    // TASK 1:
    private String title;
    private LocalDate dueDate;
    protected Status status;

    protected List<EventLog> eventLogs;

    public BoardItem(String title, LocalDate dueDate,Status status) {
        this.setTitle(title);
        this.setDueDate(dueDate);
        this.setStatus(status);
        this.eventLogs = new ArrayList<>();
        eventLogs.add(new EventLog(String.format("Item created: %s", this.viewInfo())));
    }

    public void setTitle(String title) {
        if (title.length() < 5 || title.length() > 30) {
            throw new IllegalArgumentException("Please provide a title with length between 5 and 30 chars");
        }
        if (this.title != null) {
            eventLogs.add(new EventLog(String.format("Title changed from %s to %s", this.title, title)));
        }
        this.title = title;
    }

    public void setDueDate(LocalDate dueDate) {
        if (dueDate.isBefore(LocalDate.now())) {
            throw new IllegalArgumentException("Please provide a dueDate that is not in the past");
        }
        if (this.dueDate != null) {
            eventLogs.add(new EventLog(String.format("DueDate changed from %s to %s", this.dueDate, dueDate)));
        }
        this.dueDate = dueDate;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return this.status;
    }

    public String getTitle() {
        return this.title;
    }

    public LocalDate getDueDate() {
        return this.dueDate;
    }

    // TASK 2:
    public abstract void advanceStatus();


    // TASK 2:
    public abstract void revertStatus();

    public String viewInfo() {
        return String.format("'%s', [%s | %s]", this.title, this.status, this.dueDate);
    }

    public void displayHistory() {
        StringBuilder builder = new StringBuilder();

        for (EventLog eventLog : this.eventLogs) {
            builder.append(eventLog.viewInfo()).append(System.lineSeparator());
        }
        System.out.println(builder.toString().trim());
    }
}
