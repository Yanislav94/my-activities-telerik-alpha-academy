package enums;

public enum Status {
    Open("Open"), ToDo("To Do"), InProgress("In Progress"), Done("Done"), Verified("Verified");

    private final String displayName;

    Status(String displayName) {
        this.displayName = displayName;
    }

    public String displayName() {
        return displayName;
    }
}
