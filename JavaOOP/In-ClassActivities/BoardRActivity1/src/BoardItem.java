import java.time.LocalDate;

public class BoardItem {

    // TASK 1:
    String title;
    LocalDate dueDate;
    Status status;

    // TASK 1:
    public BoardItem(String title, LocalDate dueDate) {
        this.title = title;
        this.dueDate = dueDate;
        this.status = Status.Open;
    }

    // TASK 2:
    public void advanceStatus() {
        Status[] statuses = Status.values();
        for (int i = 0; i < statuses.length; i++) {
            if (statuses[i] == this.status && i < statuses.length - 1) {
                this.status = statuses[i + 1];
                break;
            }
        }
    }

    // TASK 2:
    public void revertStatus() {
        Status[] statuses = Status.values();
        for (int i = 0; i < statuses.length; i++) {
            if (statuses[i] == this.status && i > 0) {
                this.status = statuses[i - 1];
                break;
            }
        }
    }

    // TASK 3:
    public String viewInfo() {
        return String.format("'%s', [%s | %s]", this.title, this.status, this.dueDate);
    }
}
