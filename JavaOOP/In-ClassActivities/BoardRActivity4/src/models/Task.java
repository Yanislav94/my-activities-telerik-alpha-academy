package models;

import enums.Status;

import java.time.LocalDate;

public class Task extends BoardItem {
    private static final Status INITIAL_STATUS = Status.ToDo;
    private String assignee;

    public Task(String title, String assignee, LocalDate dueDate) {
        super(title, dueDate, INITIAL_STATUS);
        this.setAssignee(assignee);
    }

    public String getAssignee() {
        return assignee;
    }


    void setAssignee(String assignee) {
        if (assignee.trim().isEmpty() || assignee.length() < 5 || assignee.length() > 30) {
            throw new IllegalArgumentException("Invalid assignee!");
        }
        if (this.assignee != null) {
            eventLogs.add(new EventLog(String.format("Title changed from %s to %s", this.assignee, assignee)));
        }
        this.assignee = assignee;
    }

    @Override
    public void advanceStatus() {
        if (this.getStatus() == Status.Verified) {
            eventLogs.add(new EventLog("Can't advance, already at Verified"));
            return;
        }
        Status[] statuses = Status.values();
        for (int i = 0; i < statuses.length; i++) {
            if (statuses[i] == this.getStatus() && i < statuses.length - 1) {
                Status newStatus = statuses[i + 1];
                eventLogs.add(new EventLog(String.format("Task status changed from %s to %s", this.getStatus().displayName(), newStatus.displayName())));
                this.setStatus(newStatus);
                break;
            }
        }
    }

    @Override
    public void revertStatus() {
        if (this.getStatus() == Status.ToDo) {
            eventLogs.add(new EventLog("Can't revert, already at To Do"));
            return;
        }
        Status[] statuses = Status.values();
        for (int i = 0; i < statuses.length; i++) {
            if (statuses[i] == this.getStatus() && i > 0) {
                Status newStatus = statuses[i - 1];
                eventLogs.add(new EventLog(String.format("Task status changed from %s to %s", this.getStatus().displayName(), newStatus.displayName())));
                this.setStatus(newStatus);
                break;
            }
        }
    }

    @Override
    public String viewInfo() {
        String baseInfo = super.viewInfo();
        return String.format("Task: %s, Assignee: %s", baseInfo, this.assignee);
    }
}
