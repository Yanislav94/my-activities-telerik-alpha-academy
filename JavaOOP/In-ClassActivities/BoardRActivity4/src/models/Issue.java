package models;

import enums.Status;

import java.time.LocalDate;

public class Issue extends BoardItem {

    private static final Status INITIAL_STATUS = Status.Open;
    private String description;

    public Issue(String title, String description, LocalDate dueDate) {
        super(title, dueDate, INITIAL_STATUS);
        this.setDescription(description);
    }

    private void setDescription(String description) {
        if (description.trim().isEmpty()) {
            this.description = "No description";
            return;
        }
        this.description = description;
    }

    @Override
    public void advanceStatus() {
        if (!this.getStatus().equals(Status.Verified)) {
            this.setStatus(Status.Verified);
            eventLogs.add(new EventLog(String.format("Issue status set to %s", this.getStatus().displayName())));
            return;
        }
        eventLogs.add(new EventLog(String.format("Issue status already %s", this.getStatus().displayName())));
    }

    @Override
    public void revertStatus() {
        if (!this.getStatus().equals(Status.Open)) {
            this.setStatus(Status.Open);
            eventLogs.add(new EventLog(String.format("Issue status set to %s", this.getStatus().displayName())));
            return;
        }
        eventLogs.add(new EventLog(String.format("Issue status already %s", this.getStatus().displayName())));
    }

    @Override
    public String viewInfo() {
        String baseInfo = super.viewInfo();
        return String.format("Issue: %s, Description: %s",baseInfo,this.description);
    }
}
