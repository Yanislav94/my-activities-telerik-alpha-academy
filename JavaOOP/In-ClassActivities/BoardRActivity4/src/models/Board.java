package models;

import contracts.Logger;

import java.util.ArrayList;
import java.util.List;

public class Board {
    private List<BoardItem> items;

    public Board() {
        this.items = new ArrayList<>();
    }

    public void addItem(BoardItem item) {
        if (this.items.contains(item)) {
            throw new IllegalArgumentException("Item already in the list");
        }
        this.items.add(item);
    }

    public void displayHistory(Logger logger) {
        for (BoardItem item : items) {
            logger.log(item.getHistory());
        }
    }

    public int totalItems() {
        return this.items.size();
    }
}
