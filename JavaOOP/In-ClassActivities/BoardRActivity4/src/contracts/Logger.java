package contracts;

public interface Logger {
    void log(String value);
}
