package com.company.oop.cosmetics.models;

import com.company.oop.cosmetics.models.contracts.Toothpaste;
import com.company.oop.cosmetics.models.enums.GenderType;
import com.company.oop.cosmetics.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

public class ToothpasteImpl implements Toothpaste {

    public static final int NAME_MIN_LENGTH = 3;
    public static final int NAME_MAX_LENGTH = 10;
    public static final int BRAND_NAME_MIN_LENGTH = 2;
    public static final int BRAND_NAME_MAX_LENGTH = 10;

    private String name;
    private String brand;
    private double price;
    private GenderType genderType;
    private List<String> ingredients;

    public ToothpasteImpl(String name, String brandName, double price, GenderType genderType, List<String> ingredients) {
        this.setName(name);
        this.setBrand(brandName);
        this.setPrice(price);
        this.setGenderType(genderType);
        this.setIngredients(ingredients);
    }

    private void setName(String name) {
        ValidationHelpers.validateStringLength(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH, "Name");
        this.name = name;
    }

    private void setBrand(String brand) {
        ValidationHelpers.validateStringLength(brand, BRAND_NAME_MIN_LENGTH, BRAND_NAME_MAX_LENGTH, "Brand");
        this.brand = brand;
    }

    private void setPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException("Price cannot be negative");
        }
        this.price = price;
    }

    private void setGenderType(GenderType genderType) {
        this.genderType = genderType;
    }

    private void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getBrand() {
        return this.brand;
    }

    @Override
    public double getPrice() {
        return this.price;
    }

    @Override
    public GenderType getGenderType() {
        return this.genderType;
    }

    @Override
    public String print() {
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("#%s %s", this.name, this.brand)).append(System.lineSeparator());
        builder.append(String.format(" #Price: $%.2f", this.price)).append(System.lineSeparator());
        builder.append(String.format(" #Gender: %s", this.genderType)).append(System.lineSeparator());
        builder.append(String.format(" #Ingredients: [%s]", String.join(", ", this.ingredients))).append(System.lineSeparator());
        return builder.toString();
    }

    @Override
    public List<String> getIngredients() {
        return new ArrayList<>(this.ingredients);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ToothpasteImpl toothpaste = (ToothpasteImpl) o;
        return getName().equals(toothpaste.getName()) &&
                getBrand().equals(toothpaste.getBrand()) &&
                getPrice() == toothpaste.getPrice() &&
                this.getGenderType().equals(toothpaste.getGenderType()) &&
                getIngredients().equals(toothpaste.getIngredients());
    }
}
