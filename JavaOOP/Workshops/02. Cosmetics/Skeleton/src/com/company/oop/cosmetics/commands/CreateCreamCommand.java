package com.company.oop.cosmetics.commands;

import com.company.oop.cosmetics.core.contracts.Command;
import com.company.oop.cosmetics.core.contracts.CosmeticsRepository;
import com.company.oop.cosmetics.models.enums.GenderType;
import com.company.oop.cosmetics.models.enums.ScentType;
import com.company.oop.cosmetics.models.enums.UsageType;
import com.company.oop.cosmetics.utils.ParsingHelpers;
import com.company.oop.cosmetics.utils.ValidationHelpers;

import java.util.List;

public class CreateCreamCommand implements Command {
    private static final String CREAM_CREATED = "Cream with name %s was created!";
    private static final String PRODUCT_ALREADY_EXISTS = "Product with name %s already exists!";

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 5;

    private final CosmeticsRepository cosmeticsRepository;

    public CreateCreamCommand(CosmeticsRepository cosmeticsRepository) {
        this.cosmeticsRepository = cosmeticsRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String name = parameters.get(0);
        String brand = parameters.get(1);
        double price = ParsingHelpers.tryParseDouble(parameters.get(2), "Invalid Price");
        GenderType genderType = ParsingHelpers.tryParseGender(parameters.get(3));
        ScentType scentType = ParsingHelpers.tryParseScent(parameters.get(4));
        return createCream(name, brand, price, genderType, scentType);
    }
    private String createCream(String name, String brand, double price, GenderType genderType, ScentType scentType) {
        try {
            cosmeticsRepository.findProductByName(name);
        } catch (IllegalArgumentException e) {
            cosmeticsRepository.createCream(name, brand, price, genderType, scentType);
            return String.format(CREAM_CREATED, name);
        }
        throw new IllegalArgumentException(String.format(PRODUCT_ALREADY_EXISTS, name));
    }


}
