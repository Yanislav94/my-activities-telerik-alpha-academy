package com.company.oop.cosmetics.models;

import com.company.oop.cosmetics.models.contracts.Cream;
import com.company.oop.cosmetics.models.enums.GenderType;
import com.company.oop.cosmetics.models.enums.ScentType;
import com.company.oop.cosmetics.utils.ValidationHelpers;

public class CreamImpl implements Cream {
    public static final int NAME_MIN_LENGTH = 3;
    public static final int NAME_MAX_LENGTH = 15;
    public static final int BRAND_NAME_MIN_LENGTH = 3;
    public static final int BRAND_NAME_MAX_LENGTH = 15;

    private String name;
    private String brand;
    private double price;
    private GenderType genderType;
    private ScentType scentType;

    public CreamImpl(String name, String brand, double price, GenderType genderType, ScentType scentType) {
        this.setName(name);
        this.setBrand(brand);
        this.setPrice(price);
        this.setGenderType(genderType);
        this.setScentType(scentType);
    }

    private void setName(String name) {
        ValidationHelpers.validateStringLength(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH, "Name");
        this.name = name;
    }

    private void setBrand(String brand) {
        ValidationHelpers.validateStringLength(brand, BRAND_NAME_MIN_LENGTH, BRAND_NAME_MAX_LENGTH, "Brand");
        this.brand = brand;
    }

    private void setPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException("Price cannot be negative");
        }
        this.price = price;
    }

    private void setGenderType(GenderType genderType) {
        this.genderType = genderType;
    }

    private void setScentType(ScentType scentType) {
        this.scentType = scentType;
    }

    @Override
    public ScentType getScentType() {
        return this.scentType;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getBrand() {
        return this.brand;
    }

    @Override
    public double getPrice() {
        return this.price;
    }

    @Override
    public GenderType getGenderType() {
        return this.genderType;
    }

    @Override
    public String print() {
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("#%s %s", this.name, this.brand)).append(System.lineSeparator());
        builder.append(String.format(" #Price: $%.2f",this.price)).append(System.lineSeparator());
        builder.append(String.format(" #Gender: %s",this.genderType)).append(System.lineSeparator());
        builder.append(String.format(" #Scent: %s",this.scentType)).append(System.lineSeparator());

        return builder.toString();
    }
}
