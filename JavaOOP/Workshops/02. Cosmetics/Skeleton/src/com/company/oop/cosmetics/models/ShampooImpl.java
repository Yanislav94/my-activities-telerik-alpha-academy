package com.company.oop.cosmetics.models;

import com.company.oop.cosmetics.models.contracts.Shampoo;
import com.company.oop.cosmetics.models.enums.GenderType;
import com.company.oop.cosmetics.models.enums.UsageType;
import com.company.oop.cosmetics.utils.ValidationHelpers;

public class ShampooImpl implements Shampoo {

    public static final int NAME_MIN_LENGTH = 3;
    public static final int NAME_MAX_LENGTH = 10;
    public static final int BRAND_NAME_MIN_LENGTH = 2;
    public static final int BRAND_NAME_MAX_LENGTH = 10;
    private String name;
    private String brand;
    private double price;
    private GenderType genderType;
    private int milliliters;
    private UsageType usageType;

    public ShampooImpl(String name, String brand, double price, GenderType genderType, int milliliters, UsageType usageType) {
        this.setName(name);
        this.setBrand(brand);
        this.setPrice(price);
        this.setGenderType(genderType);
        this.setMilliliters(milliliters);
        this.setUsageType(usageType);
    }

    public void setName(String name) {
        ValidationHelpers.validateStringLength(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH, "Name");
        this.name = name;
    }

    public void setBrand(String brand) {
        ValidationHelpers.validateStringLength(brand, BRAND_NAME_MIN_LENGTH, BRAND_NAME_MAX_LENGTH, "Brand");
        this.brand = brand;
    }

    public void setPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException("Price cannot be negative");
        }
        this.price = price;
    }

    public void setGenderType(GenderType genderType) {
        this.genderType = genderType;
    }

    public void setMilliliters(int milliliters) {
        if (milliliters < 0) {
            throw new IllegalArgumentException("Milliliters cannot be negative");
        }
        this.milliliters = milliliters;
    }

    public void setUsageType(UsageType usageType) {
        this.usageType = usageType;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getBrand() {
        return this.brand;
    }

    @Override
    public double getPrice() {
        return this.price;
    }

    @Override
    public GenderType getGenderType() {
        return this.genderType;
    }

    @Override
    public int getMillilitres() {
        return this.milliliters;
    }

    @Override
    public UsageType getUsageType() {
        return this.usageType;
    }

    @Override
    public String print() {
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("#%s %s", this.name, this.brand)).append(System.lineSeparator());
        builder.append(String.format(" #Price: $%.2f", this.price)).append(System.lineSeparator());
        builder.append(String.format(" #Gender: %s", this.genderType)).append(System.lineSeparator());
        builder.append(String.format(" #Milliliters: %d", this.milliliters)).append(System.lineSeparator());
        builder.append(String.format(" #Usage: %s", this.usageType)).append(System.lineSeparator());
        return builder.toString();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShampooImpl shampoo = (ShampooImpl) o;
        return getName().equals(shampoo.getName()) &&
                getBrand().equals(shampoo.getBrand()) &&
                getPrice() == shampoo.getPrice() &&
                getGenderType().equals(shampoo.getGenderType()) &&
                getMillilitres() == shampoo.getMillilitres() &&
                getUsageType().equals(shampoo.getUsageType());
    }
}
