package com.company.oop.dealership.models;

import com.company.oop.dealership.models.contracts.Comment;
import com.company.oop.dealership.models.contracts.Vehicle;
import com.company.oop.dealership.models.enums.VehicleType;
import com.company.oop.dealership.utils.FormattingHelpers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class BaseVehicle implements Vehicle {
    private String make;
    private String model;
    private int wheelsCount;
    private double price;

    private Collection<Comment> comments;

    public BaseVehicle(String make, String model, int wheelsCount, double price) {
        this.setMake(make);
        this.setModel(model);
        this.setWheelsCount(wheelsCount);
        this.setPrice(price);
        this.comments = new ArrayList<>();
    }


    @Override
    public String getMake() {
        return this.make;
    }

    @Override
    public String getModel() {
        return this.model;
    }

    @Override
    public int getWheels() {
        return this.wheelsCount;
    }

    @Override
    public double getPrice() {
        return this.price;
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(this.comments);
    }

    private void setMake(String make) {
        validateMakeLength(make);
        this.make = make;
    }

    private void setModel(String model) {
        validateModelLength(model);
        this.model = model;
    }

    private void setWheelsCount(int wheelsCount) {
        this.wheelsCount = wheelsCount;
    }


    private void setPrice(double price) {
        validatePriceRange(price);
        this.price = price;
    }

    @Override
    public void addComment(Comment comment) {
        this.comments.add(comment);
    }

    @Override
    public void removeComment(Comment comment) {
        this.comments.remove(comment);
    }

    protected abstract void validateMakeLength(String make);

    protected abstract void validateModelLength(String model);

    protected abstract void validatePriceRange(double price);

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("Make: %s%n", this.make));
        builder.append(String.format("Model: %s%n", this.model));
        builder.append(String.format("Wheels: %d%n", this.wheelsCount));
        builder.append(String.format("Price: $%s%n", FormattingHelpers.removeTrailingZerosFromDouble(this.price)));

        return builder.toString();
    }
}
