package com.company.oop.dealership.commands;

import com.company.oop.dealership.core.contracts.VehicleDealershipRepository;
import com.company.oop.dealership.models.contracts.User;
import com.company.oop.dealership.models.enums.UserRole;
import com.company.oop.dealership.utils.ValidationHelpers;

import java.util.List;

public class ShowUsersCommand extends BaseCommand {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;
    private final static String INVALID_USER_TYPE = "You are not an admin!";
    private final static String USER_HEADER = "--USERS--";

    public ShowUsersCommand(VehicleDealershipRepository vehicleDealershipRepository) {
        super(vehicleDealershipRepository);
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        if (!super.getVehicleDealershipRepository().getLoggedInUser().getRole().equals(UserRole.ADMIN)) {
            throw new IllegalArgumentException(INVALID_USER_TYPE);
        }
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        StringBuilder builder = new StringBuilder();
        int counter = 1;
        builder.append(USER_HEADER).append(System.lineSeparator());
        for (User user : super.getVehicleDealershipRepository().getUsers()) {
            builder.append(String.format("%d. %s", counter++, user.toString()));
        }
        return builder.toString().trim();
    }
}
