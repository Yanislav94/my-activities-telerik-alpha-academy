package com.company.oop.dealership.models;

import com.company.oop.dealership.models.contracts.Comment;
import com.company.oop.dealership.utils.ValidationHelpers;

import static java.lang.String.format;

public class CommentImpl implements Comment {

    public static final int CONTENT_LEN_MIN = 3;
    public static final int CONTENT_LEN_MAX = 200;
    private static final String CONTENT_LEN_ERR = format(
            "Content must be between %d and %d characters long!",
            CONTENT_LEN_MIN,
            CONTENT_LEN_MAX);

    private String content;
    private String author;

    public CommentImpl( String content,String author) {
        this.setAuthor(author);
        this.setContent(content);
    }

    @Override
    public String getContent() {
        return this.content;
    }

    @Override
    public String getAuthor() {
        return this.author;
    }

    private void setAuthor(String author) {
        this.author = author;
    }

    private void setContent(String content) {
        ValidationHelpers.validateStringLength(
                content
                , CONTENT_LEN_MIN
                , CONTENT_LEN_MAX
                , String.format(CONTENT_LEN_ERR, CONTENT_LEN_MIN, CONTENT_LEN_MAX));
        this.content = content;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("----------%n"));
        builder.append(String.format("%s%n", this.getContent()));
        builder.append(String.format("User: %s%n", this.getAuthor()));
        builder.append(String.format("----------%n"));
        return builder.toString();
    }
}
