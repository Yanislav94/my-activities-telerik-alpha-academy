package com.company.oop.dealership.models;

import com.company.oop.dealership.models.contracts.Comment;
import com.company.oop.dealership.models.contracts.Truck;
import com.company.oop.dealership.models.enums.VehicleType;
import com.company.oop.dealership.utils.ValidationHelpers;

import java.util.List;

import static java.lang.String.format;

public class TruckImpl extends BaseVehicle implements Truck {
    public static final int WHEELS_COUNT = 8;

    public static final int MAKE_NAME_LEN_MIN = 2;
    public static final int MAKE_NAME_LEN_MAX = 15;
    private static final String MAKE_NAME_LEN_ERR = format(
            "Make must be between %s and %s characters long!",
            MAKE_NAME_LEN_MIN,
            MAKE_NAME_LEN_MAX);
    public static final int MODEL_NAME_LEN_MIN = 1;
    public static final int MODEL_NAME_LEN_MAX = 15;
    private static final String MODEL_NAME_LEN_ERR = format(
            "Model must be between %s and %s characters long!",
            MODEL_NAME_LEN_MIN,
            MODEL_NAME_LEN_MAX);
    public static final double PRICE_VAL_MIN = 0;
    public static final double PRICE_VAL_MAX = 1000000;
    private static final String PRICE_VAL_ERR = format(
            "Price must be between %.1f and %.1f!",
            PRICE_VAL_MIN,
            PRICE_VAL_MAX);
    public static final int WEIGHT_CAP_MIN = 1;
    public static final int WEIGHT_CAP_MAX = 100;
    private static final String WEIGHT_CAP_ERR = format(
            "Weight capacity must be between %d and %d!",
            WEIGHT_CAP_MIN,
            WEIGHT_CAP_MAX);
    public static final String NO_COMMENT_OUTPUT_MSG = "--NO COMMENTS--";
    public static final String COMMENTS_HEADER_FOOTER = "--COMMENTS--";

    private int weightCapacity;

    public TruckImpl(String make, String model, double price, int weightCapacity) {
        super(make, model, WHEELS_COUNT, price);
        this.setWeightCapacity(weightCapacity);
    }

    @Override
    public int getWeightCapacity() {
        return this.weightCapacity;
    }

    @Override
    public VehicleType getType() {
        return VehicleType.TRUCK;
    }

    private void setWeightCapacity(int weightCapacity) {
        validateWeightCapacity(weightCapacity);
        this.weightCapacity = weightCapacity;
    }


    @Override
    protected void validateMakeLength(String make) {
        ValidationHelpers.validateStringLength(
                make
                , MAKE_NAME_LEN_MIN
                , MAKE_NAME_LEN_MAX
                , MAKE_NAME_LEN_ERR);
    }

    @Override
    protected void validateModelLength(String model) {
        ValidationHelpers.validateStringLength(
                model
                , MODEL_NAME_LEN_MIN
                , MODEL_NAME_LEN_MAX
                , MODEL_NAME_LEN_ERR);
    }

    @Override
    protected void validatePriceRange(double price) {
        ValidationHelpers.validateDecimalRange(
                price
                , PRICE_VAL_MIN
                , PRICE_VAL_MAX
                , PRICE_VAL_ERR);
    }

    private void validateWeightCapacity(int weightCapacity) {
        ValidationHelpers.validateIntRange(
                weightCapacity
                , WEIGHT_CAP_MIN
                , WEIGHT_CAP_MAX
                , WEIGHT_CAP_ERR);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("%s:%n", this.getType()));
        builder.append(super.toString());
        builder.append(String.format("Weight Capacity: %dt%n", this.weightCapacity));

        List<Comment> comments = this.getComments();

        if(comments.isEmpty()){
            builder.append(NO_COMMENT_OUTPUT_MSG).append(System.lineSeparator());
            return builder.toString();
        }

        builder.append(String.format(COMMENTS_HEADER_FOOTER)).append(System.lineSeparator());
        for (Comment comment : comments) {
            builder.append(comment.toString());
        }
        builder.append(String.format(COMMENTS_HEADER_FOOTER)).append(System.lineSeparator());

        return builder.toString();
    }
}
