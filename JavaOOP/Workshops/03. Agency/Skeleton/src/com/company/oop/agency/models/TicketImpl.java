package com.company.oop.agency.models;

import com.company.oop.agency.exceptions.InvalidUserInputException;
import com.company.oop.agency.models.contracts.Journey;
import com.company.oop.agency.models.contracts.Ticket;

public class TicketImpl implements Ticket {
    private int id;
    private Journey journey;
    private double cost;

    public TicketImpl(int id, Journey journey, double costs) {
        this.setId(id);
        this.setJourney(journey);
        this.setCost(costs);
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public Journey getJourney() {
        return this.journey;
    }

    @Override
    public double getAdministrativeCosts() {
        return this.cost;
    }

    private void setId(int id) {
        this.id = id;
    }

    private void setJourney(Journey journey) {
        this.journey = journey;
    }

    private void setCost(double cost) {
        if (cost < 0) {
            throw new InvalidUserInputException(String.format("Value of 'costs' must be a positive number. Actual value: %.2f.",
                    cost));
        }
        this.cost = cost;
    }

    @Override
    public double calculatePrice() {
        return this.cost * this.journey.calculateTravelCosts();
    }

    @Override
    public String getAsString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Ticket ----").append(System.lineSeparator());
        builder.append(String.format("Destination: %s", this.journey.getDestination())).append(System.lineSeparator());
        builder.append(String.format("Price: %.2f%n", this.calculatePrice()));
        return builder.toString();
    }

    @Override
    public String toString() {
        return this.getAsString();
    }
}
