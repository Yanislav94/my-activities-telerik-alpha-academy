package com.company.oop.agency.models.vehicles;

import com.company.oop.agency.models.vehicles.contracts.Airplane;

public class AirplaneImpl extends BaseVehicle implements Airplane {
    private static final VehicleType TYPE = VehicleType.AIR;
    private boolean hasFreeFood;

    public AirplaneImpl(int id, int passengerCapacity, double pricePerKilometer, boolean hasFreeFood) {
        super(id, TYPE, passengerCapacity, pricePerKilometer);
        this.hasFreeFood = hasFreeFood;
    }

    public void setHasFreeFood(boolean hasFreeFood) {
        this.hasFreeFood = hasFreeFood;
    }

    @Override
    public boolean hasFreeFood() {
        return this.hasFreeFood;
    }

    @Override
    public String getAsString() {
        return String.format("Airplane ----%n" +
                        "%s%n" +
                        "Has free food: %s%n"
                , super.getAsString()
                , this.hasFreeFood);
    }
    @Override
    public String toString() {
        return this.getAsString();
    }
}