package com.company.oop.agency.models;

import com.company.oop.agency.models.contracts.Journey;
import com.company.oop.agency.models.vehicles.contracts.Vehicle;
import com.company.oop.agency.utils.ValidationHelper;

public class JourneyImpl implements Journey {

    public static final int START_LOCATION_MIN_LENGTH = 5;
    public static final int START_LOCATION_MAX_LENGTH = 25;
    public static final int DESTINATION_MIN_LENGTH = 5;
    public static final int DESTINATION_MAX_LENGTH = 25;
    public static final int DISTANCE_MIN_VALUE = 5;
    public static final int DISTANCE_MAX_VALUE = 5000;

    private int id;
    private String startLocation;
    private String destination;
    private int distance;
    private Vehicle vehicle;

    public JourneyImpl(int id, String startLocation, String destination, int distance, Vehicle vehicle) {
        this.setId(id);
        this.setStartLocation(startLocation);
        this.setDestination(destination);
        this.setDistance(distance);
        this.setVehicle(vehicle);
    }

    private void setId(int id) {
        this.id = id;
    }

    private void setStartLocation(String startLocation) {
        ValidationHelper.validateStringLength(startLocation
                , START_LOCATION_MIN_LENGTH
                , START_LOCATION_MAX_LENGTH
                , "The StartingLocation's length cannot be less than 5 or more than 25 symbols long.");
        this.startLocation = startLocation;
    }

    private void setDestination(String destination) {
        ValidationHelper.validateStringLength(destination
                , DESTINATION_MIN_LENGTH
                , DESTINATION_MAX_LENGTH
                , "The Destination's length cannot be less than 5 or more than 25 symbols long.");
        this.destination = destination;
    }

    private void setDistance(int distance) {
        ValidationHelper.validateValueInRange(distance
                , DISTANCE_MIN_VALUE
                , DISTANCE_MAX_VALUE
                , "The Distance cannot be less than 5 or more than 5000 kilometers.");
        this.distance = distance;
    }

    private void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public String getStartLocation() {
        return this.startLocation;
    }

    @Override
    public String getDestination() {
        return this.destination;
    }

    @Override
    public int getDistance() {
        return this.distance;
    }

    @Override
    public Vehicle getVehicle() {
        return this.vehicle;
    }


    @Override
    public double calculateTravelCosts() {
        return distance * this.vehicle.getPricePerKilometer();
    }

    @Override
    public String getAsString() {
        StringBuilder builder = new StringBuilder();

        builder.append("Journey ----").append(System.lineSeparator());
        builder.append(String.format("Start location: %s", this.startLocation)).append(System.lineSeparator());
        builder.append(String.format("Destination: %s", this.destination)).append(System.lineSeparator());
        builder.append(String.format("Distance: %d", this.distance)).append(System.lineSeparator());
        builder.append(String.format("Vehicle type: %s", this.vehicle.getType())).append(System.lineSeparator());
        builder.append(String.format("Travel costs: %.2f%n", this.calculateTravelCosts()));

        return builder.toString();
    }
    @Override
    public String toString() {
        return this.getAsString();
    }

}