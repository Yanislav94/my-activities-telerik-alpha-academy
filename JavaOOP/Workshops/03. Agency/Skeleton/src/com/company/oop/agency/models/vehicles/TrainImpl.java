package com.company.oop.agency.models.vehicles;

import com.company.oop.agency.models.vehicles.contracts.Train;
import com.company.oop.agency.utils.ValidationHelper;

public class TrainImpl extends BaseVehicle implements Train {

    public static final int PASSENGER_MIN_VALUE = 30;
    public static final int PASSENGER_MAX_VALUE = 150;
    public static final int CARTS_MIN_VALUE = 1;
    public static final int CARTS_MAX_VALUE = 15;
    public static final double PRICE_MIN_VALUE = 0.1;
    public static final double PRICE_MAX_VALUE = 2.5;
    private static final VehicleType TYPE = VehicleType.LAND;
    private int carts;


    public TrainImpl(int id, int passengerCapacity, double pricePerKilometer, int carts) {
        super(id, TYPE, passengerCapacity, pricePerKilometer);
        validateCapacity(passengerCapacity);
        this.setCarts(carts);
    }

    public void setCarts(int carts) {
        validateCarts(carts);
        this.carts = carts;
    }



    @Override
    public int getCarts() {
        return this.carts;
    }

    @Override
    public String getAsString() {
        return String.format("Train ----%n" +
                        "%s%n" +
                        "Carts amount: %s%n"
                , super.getAsString()
                , this.carts);
    }

    @Override
    public String toString() {
        return this.getAsString();
    }

    @Override
    void validateCapacity(int passengerCapacity) {
        super.validateCapacity(passengerCapacity);
        ValidationHelper.validateValueInRange(passengerCapacity
                , PASSENGER_MIN_VALUE
                , PASSENGER_MAX_VALUE
                , "A train cannot have less than 30 passengers or more than 150 passengers.");

    }
    private void validateCarts(int carts) {
        ValidationHelper.validateValueInRange(carts
                , CARTS_MIN_VALUE
                , CARTS_MAX_VALUE
                , "A train cannot have less than 1 cart or more than 15 carts.");
    }
}