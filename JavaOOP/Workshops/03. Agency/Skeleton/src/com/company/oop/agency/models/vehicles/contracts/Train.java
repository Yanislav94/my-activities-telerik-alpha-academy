package com.company.oop.agency.models.vehicles.contracts;

import com.company.oop.agency.models.contracts.Identifiable;

public interface Train extends Vehicle {

    int getCarts();

}