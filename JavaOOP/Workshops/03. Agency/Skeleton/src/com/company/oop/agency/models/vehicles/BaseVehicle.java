package com.company.oop.agency.models.vehicles;

import com.company.oop.agency.models.vehicles.contracts.Vehicle;
import com.company.oop.agency.utils.ValidationHelper;

public abstract class BaseVehicle implements Vehicle {
    public static final int PASSENGER_MIN_VALUE = 1;
    public static final int PASSENGER_MAX_VALUE = 800;
    public static final double PRICE_MIN_VALUE = 0.1;
    public static final double PRICE_MAX_VALUE = 2.5;

    private int id;
    private VehicleType type;
    private int passengerCapacity;
    private double pricePerKilometer;

    public BaseVehicle(int id, VehicleType type, int passengerCapacity, double pricePerKilometer) {
        this.setId(id);
        this.setType(type);
        this.setPassengerCapacity(passengerCapacity);
        this.setPricePerKilometer(pricePerKilometer);
    }

    private void setId(int id) {
        this.id = id;
    }

    private void setType(VehicleType type) {
        this.type = type;
    }

    private void setPassengerCapacity(int passengerCapacity) {
        validateCapacity(passengerCapacity);
        this.passengerCapacity = passengerCapacity;
    }

    private void setPricePerKilometer(double pricePerKilometer) {
        validatePrice(pricePerKilometer);
        this.pricePerKilometer = pricePerKilometer;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public VehicleType getType() {
        return this.type;
    }

    @Override
    public int getPassengerCapacity() {
        return this.passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return this.pricePerKilometer;
    }

    @Override
    public String getAsString() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("Passenger capacity: %d", this.passengerCapacity)).append(System.lineSeparator());
        builder.append(String.format("Price per kilometer: %.2f", this.pricePerKilometer)).append(System.lineSeparator());
        builder.append(String.format("Vehicle type: %s", this.type)).append(System.lineSeparator());
        return builder.toString().trim();
    }

    @Override
    public String toString() {
        return this.getAsString();
    }

    void validateCapacity(int passengerCapacity) {
        ValidationHelper.validateValueInRange(passengerCapacity
                , PASSENGER_MIN_VALUE
                , PASSENGER_MAX_VALUE
                , "A vehicle with less than 1 passengers or more than 800 passengers cannot exist!");
    }
    void validatePrice(double pricePerKilometer){
        ValidationHelper.validateValueInRange(pricePerKilometer
                , PRICE_MIN_VALUE
                , PRICE_MAX_VALUE
                , "A vehicle with a price per kilometer lower than $0.10 or higher than $2.50 cannot exist!");
    }
}
