package com.company.oop.agency.models.vehicles;

import com.company.oop.agency.models.vehicles.contracts.Bus;
import com.company.oop.agency.utils.ValidationHelper;

public class BusImpl extends BaseVehicle implements Bus {

    public static final int PASSENGER_MIN_VALUE = 10;
    public static final int PASSENGER_MAX_VALUE = 50;
    private static final VehicleType TYPE = VehicleType.LAND;

    public BusImpl(int id, int passengerCapacity, double pricePerKilometer) {
        super(id, TYPE, passengerCapacity, pricePerKilometer);
        validateCapacity(passengerCapacity);
    }

    @Override
    public String getAsString() {
        return String.format("Bus ----%n" +
                        "%s%n"
                , super.getAsString());
    }

    @Override
    void validateCapacity(int passengerCapacity) {
        super.validateCapacity(passengerCapacity);
        ValidationHelper.validateValueInRange(passengerCapacity
                , PASSENGER_MIN_VALUE
                , PASSENGER_MAX_VALUE
                , "A bus cannot have less than 10 passengers or more than 50 passengers.");
    }

    @Override
    public String toString() {
        return this.getAsString();
    }
}