package tests.commands;

import com.company.oop.cosmetics.commands.RemoveFromCategoryCommand;
import com.company.oop.cosmetics.commands.contracts.Command;
import com.company.oop.cosmetics.core.CosmeticsRepositoryImpl;
import com.company.oop.cosmetics.core.contracts.CosmeticsRepository;
import com.company.oop.cosmetics.models.Category;
import com.company.oop.cosmetics.models.Product;
import tests.models.ProductTests;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tests.models.CategoryTests;
import tests.utils.TestUtilities;

import java.util.List;

public class RemoveFromCategoryCommandTests {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private Command removeFromCategoryCommand;
    private CosmeticsRepository cosmeticsRepository;

    @BeforeEach
    public void before() {
        cosmeticsRepository = new CosmeticsRepositoryImpl();
        removeFromCategoryCommand = new RemoveFromCategoryCommand(cosmeticsRepository);
    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = TestUtilities.getList(EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> removeFromCategoryCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_CategoryNameIsInvalid() {
        // Arrange
        List<String> params = List.of(CategoryTests.INVALID_CATEGORY_NAME, ProductTests.VALID_PRODUCT_NAME);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> removeFromCategoryCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_CategoryDoesNotExist() {
        // Arrange
        Product product = ProductTests.addInitializedProductToRepo(cosmeticsRepository);
        List<String> params = List.of(CategoryTests.VALID_CATEGORY_NAME, product.getName());

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> removeFromCategoryCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_ProductDoesNotExist() {
        // Arrange
        Category category = CategoryTests.addInitializedCategoryToRepo(cosmeticsRepository);
        List<String> params = List.of(category.getName(), ProductTests.VALID_PRODUCT_NAME);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> removeFromCategoryCommand.execute(params));
    }

    @Test
    public void should_RemoveFromCategory_When_ArgumentsAreValid() {
        // Arrange
        Product product = ProductTests.addInitializedProductToRepo(cosmeticsRepository);
        Category category = CategoryTests.addInitializedCategoryToRepo(cosmeticsRepository);
        category.addProduct(product);
        List<String> params = List.of(category.getName(), product.getName());
        removeFromCategoryCommand.execute(params);

        // Act, Assert
        Assertions.assertEquals(0, category.getProducts().size());
    }
}
