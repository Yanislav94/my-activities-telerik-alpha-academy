package com.company.oop.cosmetics.models;

import com.company.oop.cosmetics.utils.ValidationHelpers;

import java.util.Objects;

public class Product {

    public static final int NAME_MIN_LENGTH = 3;
    public static final int NAME_MAX_LENGTH = 10;
    public static final int BRAND_MIN_LENGTH = 2;
    public static final int BRAND_MAX_LENGTH = 10;

    // "Each product in the system has name, brand, price and gender."
    private String name;
    private String brand;
    private double price;
    private GenderType gender;


    public Product(String name, String brand, double price, GenderType gender) {
        // finish the constructor and validate data
        this.setName(name);
        this.setBrand(brand);
        setPrice(price);
        this.gender = gender;
    }

    public void setName(String name) {
        ValidationHelpers.validateStringLength(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH, "");
        this.name = name;
    }

    public void setBrand(String brand) {
        ValidationHelpers.validateStringLength(brand, BRAND_MIN_LENGTH, BRAND_MAX_LENGTH, "");
        this.brand = brand;
    }

    public void setPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException();
        }
        this.price = price;
    }

    public String getName() {
        return this.name;
    }

    public double getPrice() {
        return this.price;
    }

    public String getBrand() {
        return this.brand;
    }

    public GenderType getGender() {
        return this.gender;
    }

    public String print() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("#%s %s", this.name, this.brand)).append(System.lineSeparator());
        builder.append(String.format("#Price: %.2f", this.price)).append(System.lineSeparator());
        builder.append(String.format("#Gender: %s", this.gender)).append(System.lineSeparator());
        builder.append("===");
        // Format:
        //" #[Name] [Brand]
        // #Price: [Price]
        // #Gender: [Gender]
        // ==="
        return builder.toString();
    }

    @Override
    public boolean equals(Object p) {
        if (this == p) return true;
        if (p == null || getClass() != p.getClass()) return false;
        Product product = (Product) p;
        return Double.compare(this.getPrice(), product.getPrice()) == 0 &&
                Objects.equals(this.getName(), product.getName()) &&
                Objects.equals(this.getBrand(), product.getBrand()) &&
                this.getGender() == product.getGender();
    }

}
