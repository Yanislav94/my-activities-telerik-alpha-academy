package com.company.oop.cosmetics.models;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

    private List<Product> products;

    public ShoppingCart() {
        this.products = new ArrayList<>();
    }

    public List<Product> getProducts() {
        return new ArrayList<>(products);
    }

    public void addProduct(Product product) {
        this.products.add(product);
    }

    public void removeProduct(Product product) {
        if (!this.products.contains(product)) {
            throw new IllegalArgumentException();
        }
        this.products.remove(product);
    }

    public boolean containsProduct(Product product) {
        return this.products.contains(product);
    }

    public double totalPrice() {
        double total = 0;

        for (Product product : this.products) {
            total += product.getPrice();
        }
        return total;
    }

}
