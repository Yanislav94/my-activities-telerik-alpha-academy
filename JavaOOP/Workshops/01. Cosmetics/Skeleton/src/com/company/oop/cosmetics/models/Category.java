package com.company.oop.cosmetics.models;

import com.company.oop.cosmetics.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

public class Category {
    public static final int NAME_MIN_LENGTH = 2;
    public static final int NAME_MAX_LENGTH = 15;
    private String name;
    private List<Product> products;

    public Category(String name) {
        this.setName(name);
        this.products = new ArrayList<>();
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        ValidationHelpers.validateStringLength(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH, "");
        this.name = name;
    }

    public List<Product> getProducts() {
        return this.products;
    }

    public void addProduct(Product product) {
        this.products.add(product);
    }

    public void removeProduct(Product product) {
        if (!this.products.contains(product)) {
            throw new IllegalArgumentException();
        }
        this.products.remove(product);
    }

    public String print() {
        StringBuilder builder = new StringBuilder();

        builder.append("#Category: ").append(this.name).append(System.lineSeparator());
        for (Product product : this.products) {
            builder.append(product.toString());
        }
        return builder.toString();
    }

}
