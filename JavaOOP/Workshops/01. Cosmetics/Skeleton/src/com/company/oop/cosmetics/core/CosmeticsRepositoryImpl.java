package com.company.oop.cosmetics.core;

import com.company.oop.cosmetics.core.contracts.CosmeticsRepository;
import com.company.oop.cosmetics.models.Category;
import com.company.oop.cosmetics.models.GenderType;
import com.company.oop.cosmetics.models.Product;
import com.company.oop.cosmetics.models.ShoppingCart;

import java.util.ArrayList;
import java.util.List;

public class CosmeticsRepositoryImpl implements CosmeticsRepository {


    private final List<Product> products;
    private final List<Category> categories;
    private final ShoppingCart shoppingCart;

    public CosmeticsRepositoryImpl() {
        products = new ArrayList<>();
        categories = new ArrayList<>();

        shoppingCart = new ShoppingCart();
    }

    @Override
    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    @Override
    public List<Category> getCategories() {
        return new ArrayList<>(categories);
    }

    @Override
    public List<Product> getProducts() {
        return new ArrayList<>(products);
    }

    @Override
    public Product findProductByName(String productName) {
        /**
         * Hint: You have to go through every product and see if one has name equal to productName.
         *       If not, "throw new IllegalArgumentException("Product with this name does not exist");"
         */
        Product productMatchingName = this.products.stream().filter(product -> product.getName().equals(productName)).findAny().orElse(null);
        if (productMatchingName == null) {
            throw new IllegalArgumentException("Product with this name does not exist");
        }
        return productMatchingName;
    }

    @Override
    public Category findCategoryByName(String categoryName) {
        /**
         * Hint: You have to go through every category and see if one has name equal to categoryName.
         *       If not, "throw new IllegalArgumentException("Category with this name does not exist");"
         */
        Category categoryMatchingName = this.categories.stream().filter(category -> category.getName().equals(categoryName)).findAny().orElse(null);
        if (categoryMatchingName == null) {
            throw new IllegalArgumentException("Category with this name does not exist");
        }
        return categoryMatchingName;
    }

    @Override
    public void createCategory(String categoryName) {
        this.categories.add(new Category(categoryName));
    }

    @Override
    public void createProduct(String name, String brand, double price, GenderType gender) {
        this.products.add(new Product(name, brand, price, gender));
    }

    @Override
    public boolean categoryExist(String categoryName) {
        /**
         * Hint: You have to go through every category and see if one has name equal to categoryName.
         *       If there is one, return true. If not, return false.
         */
        try {
            this.findCategoryByName(categoryName);
        } catch (IllegalArgumentException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean productExist(String productName) {
        /**
         * Hint: You have to go through every product and see if one has name equal to productName.
         *       If there is one, return true. If not, return false.
         */
        try {
            this.findProductByName(productName);
        } catch (IllegalArgumentException e) {
            return false;
        }
        return true;
    }
}
