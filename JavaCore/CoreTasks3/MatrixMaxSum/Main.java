package JavaCore.CoreTasks3.MatrixMaxSum;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int rows = Integer.parseInt(sc.nextLine());
        int[][] matrix = getPopulatedMatrix(sc, rows);
        int bestSum = Integer.MIN_VALUE;

        int[] coordinates = Arrays.stream(sc.nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();

        for (int i = 0; i < coordinates.length - 1; i += 2) {
            int currentSum = 0;
            int targetRow = coordinates[i];
            int targetCol = coordinates[i + 1];

            int absTargetRow = Math.abs(targetRow) - 1;
            int absTargetCol = Math.abs(targetCol) - 1;

            int horizontalDirection = targetRow > 0 ? -1 : 1;
            int verticalDirection = targetCol > 0 ? -1 : 1;

            int currentCol = absTargetCol;

            while (isInside(absTargetRow, currentCol, matrix)) {
                currentSum += matrix[absTargetRow][currentCol];
                currentCol += horizontalDirection;
            }

            int currentRow = absTargetRow;
            while (isInside(currentRow, absTargetCol, matrix)) {
                currentSum += matrix[currentRow][absTargetCol];
                currentRow += verticalDirection;
            }

            currentSum -= matrix[absTargetRow][absTargetCol];

            if (currentSum > bestSum) {
                bestSum = currentSum;
            }


        }
        System.out.println(bestSum);
    }


    private static int[][] getPopulatedMatrix(Scanner sc, int rows) {
        int[][] populatedMatrix = new int[rows][];
        for (int row = 0; row < populatedMatrix.length; row++) {
            populatedMatrix[row] = Arrays.stream(sc.nextLine()
                            .split("\\s+"))
                    .mapToInt(Integer::parseInt)
                    .toArray();
        }
        return populatedMatrix;
    }

    private static boolean isInside(int row, int col, int[][] matrix) {
        return row >= 0 && row < matrix.length && col >= 0 && col < matrix[row].length;
    }
}
