package JavaCore.CoreTasks3.SpiralMatrix;

import java.util.Scanner;

public class Main {
    public static int counter;
    public static int currentRow;
    public static int currentCol;
    public static int[][] matrix;

    public static void main(String[] args) {
        int size = new Scanner(System.in).nextInt();

        matrix = new int[size][size];
        currentRow = 0;
        currentCol = 0;
        int horizontalMove;
        int verticalMove;
        counter = 1;
        matrix[currentRow][currentCol] = counter++;

        while (counter <= size * size) {
            horizontalMove = 1;
            verticalMove = 0;
            assignValues(horizontalMove, verticalMove);
            horizontalMove = 0;
            verticalMove = 1;
            assignValues(horizontalMove, verticalMove);
            horizontalMove = -1;
            verticalMove = 0;
            assignValues(horizontalMove, verticalMove);
            horizontalMove = 0;
            verticalMove = -1;
            assignValues(horizontalMove, verticalMove);
        }
        System.out.println(printMatrix(matrix));
    }

    private static void assignValues(int horizontalMove, int verticalMove) {
        while (isInside(currentRow + verticalMove, currentCol + horizontalMove, matrix)
                && matrix[currentRow + verticalMove][currentCol + horizontalMove] == 0) {
            currentRow += verticalMove;
            currentCol += horizontalMove;
            matrix[currentRow][currentCol] = counter++;
        }
    }

    private static boolean isInside(int row, int col, int[][] matrix) {
        return row >= 0 && row < matrix.length && col >= 0 && col < matrix[row].length;
    }

    private static String printMatrix(int[][] matrix) {
        StringBuilder builder = new StringBuilder();
        for (int[] ints : matrix) {
            for (int anInt : ints) {
                builder.append(anInt).append(" ");
            }
            builder.append(System.lineSeparator());
        }
        return builder.toString();
    }
}
