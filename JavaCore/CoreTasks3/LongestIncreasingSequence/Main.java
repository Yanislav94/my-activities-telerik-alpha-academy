package JavaCore.CoreTasks3.LongestIncreasingSequence;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        int[] arr = new int[n];

        for (int i = 0; i < n; i++) {
            arr[i] = Integer.parseInt(sc.nextLine());
        }


        int bestMax = 1;
        for (int i = 0; i < arr.length - 1; i++) {
            int currentMax = 1;
            int currentNumber = arr[i];
            int nextNumber = arr[i + 1];
            while (currentNumber < nextNumber) {
                currentMax++;

                i++;
                if (i >= arr.length - 1) {
                    break;
                }
                currentNumber = nextNumber;
                nextNumber = arr[i + 1];
            }
            if (currentMax > bestMax) {
                bestMax = currentMax;
            }
        }
        System.out.println(bestMax);
    }
}
