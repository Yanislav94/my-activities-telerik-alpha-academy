package JavaCore.CoreTasks3.ZigZag;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int[] dimensions = Arrays.stream(new Scanner(System.in).nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();

        int rows = dimensions[0];
        int cols = dimensions[1];

        long sum = 0;
        long startNumber = 1;
        for (int row = 0; row < rows; row += 2) {
            long currentNumber = startNumber;
            int totalCountOfCurrentNumber = Math.min(row + 1, cols);
            sum += totalCountOfCurrentNumber * currentNumber + Math.max(0, totalCountOfCurrentNumber - 2) * currentNumber;
            startNumber += 6;
        }
        int start = rows % 2 == 0 ? 1 : 2;
        for (int col = start; col < cols; col += 2) {
            long currentNumber = startNumber;
            int totalCountOfCurrentNumber = Math.min(rows, cols - col);
            sum += totalCountOfCurrentNumber * currentNumber + Math.max(0, totalCountOfCurrentNumber - 2) * currentNumber;
            startNumber += 6;
        }
        System.out.println(sum);
    }
}
