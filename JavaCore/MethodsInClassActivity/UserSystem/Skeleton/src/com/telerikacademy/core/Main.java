package JavaCore.MethodsInClassActivity.UserSystem.Skeleton.src.com.telerikacademy.core;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String ANSI_RESET = "\u001B[0m";
        String ANSI_BLACK = "\u001B[30m";
        String ANSI_RED = "\u001B[31m";
        String ANSI_GREEN = "\u001B[32m";

        Scanner scanner = new Scanner(System.in);
        String command = scanner.nextLine();
        String[][] usersData = new String[4][2];

        // main loop
        while (!command.equals("end")) {
            String[] commandArgs = command.split(" ");
            switch (commandArgs[0]) {
                case "register": {
                    // validate arguments
                    if (!argumentsAreValid(commandArgs.length)) {
                        System.out.println(ANSI_RED + "Too few parameters." + ANSI_RESET);
                        break;
                    }

                    String username = commandArgs[1];
                    String password = commandArgs[2];

                    // validate username
                    if (!isValidUsername(username)) {
                        System.out.println(ANSI_RED + "Username must be at least 3 characters long." + ANSI_RESET);
                        break;
                    }

                    // validate password
                    if (!isValidPassword(password)) {
                        System.out.println(ANSI_RED + "Password must be at least 3 characters long." + ANSI_RESET);
                        break;
                    }

                    // check if username exists
                    boolean usernameExists = isUsernameExists(usersData, username);

                    if (usernameExists) {
                        System.out.println(ANSI_RED + "Username already exists." + ANSI_RESET);
                        break;
                    }

                    // find free slot
                    int freeSlotIndex = getFreeSlotIndex(usersData);

                    // no free slots
                    if (freeSlotIndex == -1) {
                        System.out.println(ANSI_RED + "The system supports a maximum number of 4 users." + ANSI_RESET);
                        break;
                    }

                    String[] userArgs = usersData[freeSlotIndex];
                    // save user
                    saveUser(username, password, userArgs);

                    System.out.println(ANSI_GREEN + "Registered user." + ANSI_RESET);

                    break;
                }
                case "delete": {
                    // validate arguments
                    if (!argumentsAreValid(commandArgs.length)) {
                        System.out.println(ANSI_RED + "Too few parameters." + ANSI_RESET);
                        break;
                    }

                    String username = commandArgs[1];
                    String password = commandArgs[2];

                    // validate username
                    if (!isValidUsername(username)) {
                        System.out.println(ANSI_RED + "Username must be at least 3 characters long" + ANSI_RESET);
                        break;
                    }

                    // validate password
                    if (!isValidPassword(password)) {
                        System.out.println(ANSI_RED + "Password must be at least 3 characters long." + ANSI_RESET);
                        break;
                    }

                    // find account to delete
                    int accountIndex = getAccountIndex(usersData, username, password);

                    if (accountIndex == -1) {
                        System.out.println(ANSI_RED + "Invalid account/password." + ANSI_RESET);
                        break;
                    }

                    String[] userArgs = usersData[accountIndex];

                    deleteUser(userArgs);

                    System.out.println(ANSI_GREEN + "Deleted account." + ANSI_RESET);

                    break;
                }
            }

            // read next command
            command = scanner.nextLine();
        }
    }

    private static void saveUser(String username, String password, String[] usersData) {
        usersData[0] = username;
        usersData[1] = password;
    }

    private static void deleteUser(String[] usersData) {
        usersData[0] = null;
        usersData[1] = null;
    }

    private static int getAccountIndex(String[][] usersData, String username, String password) {
        for (int i = 0; i < usersData.length; i++) {
            if (username.equals(usersData[i][0]) && password.equals(usersData[i][1])) {
                return i;
            }
        }
        return -1;
    }

    private static boolean argumentsAreValid(int commandArgs) {
        return commandArgs >= 3;
    }

    private static boolean isValidPassword(String password) {
        return password.length() >= 3;
    }

    private static boolean isValidUsername(String username) {
        return username.length() >= 3;
    }

    private static int getFreeSlotIndex(String[][] usersData) {
        for (int i = 0; i < usersData.length; i++) {
            if (usersData[i][0] == null) {
                return i;
            }
        }
        return -1;
    }

    private static boolean isUsernameExists(String[][] usersData, String username) {
        for (String[] usersDatum : usersData) {
            if (username.equals(usersDatum[0])) {
                return true;
            }
        }
        return false;
    }
}
